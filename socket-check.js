const config = require('./config');

const type = 'dev';

const url =
    type === 'PROD'
        ? 'https://socket.delivery.zervx.com'
        : `ws://localhost:${config.SOCKET_PORT}`;

const adminConnect = () => {
    const io = require('socket.io-client');
    const adminToken =
        'T1RCaE1EYzFZamN6TjJVNE9UTTBaakkxWWpreE56ZzFaVGcxTVRaaE56WXdZV0prWkRFNE1Ea3lZekJtWXpRMk9ERmtPV1JtTXpsaU1EWTVPV05tWWc6ZXlKZmFXUWlPaUkyTVRoaVlXWTNNbU5sWW1Jd04yWm1OVFEyTmpaaFpETWlMQ0oxYzJWeVZIbHdaU0k2SWs5UVJWSkJWRTlTVXlJc0ltTnlaV0YwWldSQmRDSTZJakl3TWpFdE1USXRNakZVTURZNk5URTZOVGN1TkRrNFdpSjk';
    const admin = io(`${url}/admins`, {
        transports: ['websocket'],
        query: {
            accessToken: adminToken,
            deviceId: adminToken,
        },
    });
    admin.onAny((event, ...args) => {
        console.log(admin.id);
        console.log(`Admin Socket : ${event}`);
        console.log(args[0]);
    });
};

const shopSocket = () => {
    const io = require('socket.io-client');
    const adminToken =
        'WXpCaE1EZG1aVFU0WldVeVptTmlOR1ExTTJNMk56aGxPVFZqTnpKa01EazJNamN6TURkbFkyUXpOell3Wm1SaE5EVmtaakV5TVRsbVlURXdOR0V5WWc6ZXlKZmFXUWlPaUkyTVdRMU5qSTRabUU0Wm1aaE1UWXpNVGhtTmpGa01tWWlMQ0oxYzJWeVZIbHdaU0k2SWxOSVQxQlRJaXdpWTNKbFlYUmxaRUYwSWpvaU1qQXlNaTB3TVMwd05sUXdOam8wTmpvd05DNHlNemhhSW4w';
    const admin = io(`${url}/shops`, {
        transports: ['websocket'],
        query: {
            accessToken: adminToken,
            deviceId: adminToken,
        },
    });
    admin.onAny((event, ...args) => {
        console.log(admin.id);
        console.log(`Shop Socket : ${event}`);
        console.log(args[0]);
    });
};

const userConnect = () => {
    const io = require('socket.io-client');
    const userToken =
        'TkRBMk1tSTRZV1psTnpFeFlqazFaREJsWW1GbE5HRmpPV1UzTkRnME9XUXlZVFUyTlRRNE5tUXdZell4WVRObU9UQmtPVEV4WVRVMU1tWXpOamt6T0E6ZXlKZmFXUWlPaUkyTVRjMk16RTJPVEZsTmpZeE1qZzJObUV3TW1JellXTWlMQ0owZVhCbElqb2lWVk5GVWlJc0ltUmxkbWxqWlZSNWNHVWlPaUpKVDFNaUxDSndiR0YwWm05eWJTSTZJa2xQVXk5UWIzTjBiV0Z1VW5WdWRHbHRaUzgzTGpJNExqUWlMQ0pqY21WaGRHVmtRWFFpT2lJeU1ESXlMVEF4TFRJMVZERXlPakE0T2pBd0xqUTVPVm9pZlE';

    const user = io(`${url}/deliveryUsers`, {
        transports: ['websocket'],
        query: {
            accessToken: userToken,
            deviceId: userToken,
        },
    });
    user.onAny((event, ...args) => {
        console.log(`User Socket : ${event}`);
        console.log(args[0]);
        if (event === 'connection') {
            user.emit('TRACK_PARTNER_LOCATION', {
                partnerId: '61d6755edebcc34c49af73f9',
            });
        }
    });
};

const partnerSocket = () => {
    const io = require('socket.io-client');
    const userToken =
        'WldKbVpUWXdOVEkwWTJKbU5XTXpNRFkyTTJVMlptSmpaR0UzTlRGbU1HWm1PVGc0TVRRd00yUmtNalE1WVdObE5EQTFPRE0xTmpKaFl6aGhPRGRoWWc6ZXlKZmFXUWlPaUkyTVdRMk56VTFaV1JsWW1Oak16UmpORGxoWmpjelpqa2lMQ0owZVhCbElqb2lVRUZTVkU1RlVpSXNJbVJsZG1salpWUjVjR1VpT2lKWFJVSWlMQ0p3YkdGMFptOXliU0k2SWxkRlFpOVFiM04wYldGdVVuVnVkR2x0WlM4M0xqSTRMalFpTENKamNtVmhkR1ZrUVhRaU9pSXlNREl5TFRBeExUTXhWREV4T2pReU9qSXhMamt3TUZvaWZR';

    // const link = 'https://socket.delivery.zervx.com/partners'
    const link = `${url}/partners`;

    const user = io(link, {
        transports: ['websocket'],
        query: {
            accessToken: userToken,
            deviceId: userToken,
        },
    });

    const locations = [
        [13.0012, 80.2565],
        [13.0418, 80.2341],
        [13.0067, 80.2206],
    ];

    user.onAny((event, ...args) => {
        if (event === 'connection') {
            setInterval(() => {
                const i = Math.floor(Math.random() * locations.length);
                user.emit('UPDATE_LOCATION', {
                    lat: locations[i][0],
                    lng: locations[i][1],
                });
            }, 3000);
        }
        console.log(`Partner Socket : ${event}`);
        console.log(args[0]);
    });
};

// adminConnect();
userConnect();
// partnerSocket();
// shopSocket();
