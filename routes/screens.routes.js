const express = require('express');
const multer = require('multer');
const upload = multer();
const router = express.Router();

const controller = require('../controllers/screens.controller');
const validation = require('../validations/screens.validation');

const { validateBody } = require('../utils/errors.utils');
const { adminTokenVerify } = require('../utils/auth.utils');
const { paginationParams } = require('../utils/helper.utils');

// WalkThrough
router.post('/getWalkThrough', adminTokenVerify, controller.getWalkthrough);
router.post('/getAllWalkThrough', adminTokenVerify, controller.getAllWalkThrough);
router.post('/updateWalkThrough', adminTokenVerify, validateBody(validation.updateWalkThrough), controller.updateWalkThrough);
router.post('/uploadWalkThroughImage', adminTokenVerify, upload.single('image'), controller.uploadWalkThroughImage);

// HomeScreen
router.post('/updateHomeScreen', adminTokenVerify, validateBody(validation.updateHomeScreen), controller.updateHomeScreen);

// Mobile
router.post('/getMobileWalkThrough', controller.getMobileWalkThrough);
router.post('/getCityAndHomeScreen', validateBody(validation.getCityAndHomeScreen), controller.getCityAndHomeScreen);

module.exports = router;
