const express = require('express');
const router = express.Router();

const controller = require('../controllers/activities.controller');
const validation = require('../validations/activities.validation.js');

const { validateBody } = require('../utils/errors.utils');
const { adminTokenVerify } = require('../utils/auth.utils');
const { paginationParams } = require('../utils/helper.utils');

// Logins
router.post('/allActivities', adminTokenVerify, paginationParams, controller.allActivities);
router.post('/getAllUserActivities', adminTokenVerify, paginationParams, controller.getAllUserActivities);
router.post('/getSingleUserActivities', adminTokenVerify, paginationParams, controller.getSingleUserActivities);
router.post('/getGeneralActivites', adminTokenVerify, paginationParams, controller.getGeneralActivites);
router.post('/getCityActivities', adminTokenVerify, paginationParams, controller.getCityActivities);

module.exports = router;
