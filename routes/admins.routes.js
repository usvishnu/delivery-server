const express = require('express');
const router = express.Router();

const multer = require('multer');
const upload = multer();

// Multi Image Upload
const generalUploadImages = upload.fields([{ name: 'darkLogo' }, { name: 'lightLogo' }, { name: 'favicon' }]);

const controller = require('../controllers/admin.controller');
const validation = require('../validations/admin.validation.js');

const { validateBody } = require('../utils/errors.utils');
const { adminTokenVerify, developerVerify } = require('../utils/auth.utils');
const { paginationParams } = require('../utils/helper.utils');

router.post('/webHook', controller.webHook);

router.get('/getConfig', controller.getConfig);
router.get('/getSocketKeysInfo', controller.getSocketKeysInfo);
router.post('/getAllConfig', adminTokenVerify, controller.getAllConfig);
router.get('/getSpecification', controller.getSpecification);
router.get('/useragent', controller.useragent);
router.post('/checkError', validateBody(validation.checkError), controller.checkError);

// Logins
router.post('/loginWithPhone', validateBody(validation.loginWithPhone), controller.loginWithPhone);
router.post('/loginWithEmail', validateBody(validation.loginWithEmail), controller.loginWithEmail);
router.post('/loginValidation', adminTokenVerify, controller.loginValidation);

// Config
router.post('/updateGeneralSettings', adminTokenVerify, validateBody(validation.updateGeneralSettings), controller.updateGeneralSettings);
router.post('/uploadGeneralSettingsImages', adminTokenVerify, generalUploadImages, controller.uploadGeneralSettingsImages);

// Forgot Password
router.post('/forgotPasswordWithEmail', validateBody(validation.forgotPasswordWithEmail), controller.forgotPasswordWithEmail);
router.post('/verifyOTPWithEmail', validateBody(validation.verifyOTPWithEmail), controller.verifyOTPWithEmail);
router.post('/resetPasswordWithEmail', validateBody(validation.resetPasswordWithEmail), controller.resetPasswordWithEmail);

// Personal
router.post('/dashboard', adminTokenVerify, controller.dashboard);
router.post('/onlineDetails', adminTokenVerify, controller.onlineDetails);
router.post('/changePreferredTheme', adminTokenVerify, validateBody(validation.changePreferredTheme), controller.changePreferredTheme);
router.post('/updatePassword', adminTokenVerify, validateBody(validation.updatePassword), controller.updatePassword);
router.post('/updateProfile', adminTokenVerify, validateBody(validation.updateProfile), controller.updateProfile);
router.post('/updateProfilePicture', adminTokenVerify, upload.single('avatar'), controller.updateProfilePicture);
router.post('/getProfile', adminTokenVerify, controller.getProfile);
router.post('/updateProfileLanguage', adminTokenVerify, validateBody(validation.updateProfileLanguage), controller.updateProfileLanguage);
router.post('/restartApp', adminTokenVerify, developerVerify, validateBody(validation.restartApp), controller.restartApp);
router.post('/getNotifications', adminTokenVerify, paginationParams, controller.getNotifications);
router.post('/updateExpoToken', adminTokenVerify, controller.updateExpoToken);

// Operators
router.post('/addOperator', adminTokenVerify, upload.single('avatar'), validateBody(validation.addOperator), controller.addOperator);
router.post('/getOperator', adminTokenVerify, validateBody(validation.getOperator), controller.getOperator);
router.post('/editGetOperator', adminTokenVerify, validateBody(validation.editGetOperator), controller.editGetOperator);
router.post('/getAllOperators', adminTokenVerify, paginationParams, validateBody(validation.getAllOperators), controller.getAllOperators);
router.post('/statusChangeOperator', adminTokenVerify, validateBody(validation.statusChangeOperator), controller.statusChangeOperator);
router.post('/operatorToDeveloper', adminTokenVerify, developerVerify, controller.operatorToDeveloper);

// ONLY FOR DEVELOPMENT
router.post('/jobsList', adminTokenVerify, controller.jobsList);
router.get('/timezoneList', controller.timezoneList);
router.post('/socketLists', adminTokenVerify, controller.socketLists);
router.post('/socketEmit', adminTokenVerify, validateBody(validation.socketEmit), controller.socketEmit);

// MOBILE
router.post('/getMobileConfig', controller.getMobileConfig);

module.exports = router;
