const express = require('express');
const multer = require('multer');
const upload = multer();
const router = express.Router();

const controller = require('../controllers/cities.controller');
const validation = require('../validations/cities.validation');

const { validateBody } = require('../utils/errors.utils');
const { adminTokenVerify } = require('../utils/auth.utils');
const { paginationParams } = require('../utils/helper.utils');

router.post('/getAllCities', adminTokenVerify, paginationParams, controller.getAllCities);
router.post('/getAllCitiesLocations', adminTokenVerify, controller.getAllCitiesLocations);
router.post('/addCity', adminTokenVerify, validateBody(validation.addCity), controller.addCity);
router.post('/getCity', adminTokenVerify, controller.getCity);

// zones
router.post('/getAllZones', adminTokenVerify, paginationParams, controller.getAllZones);
router.post('/getAllZonesPolygons', adminTokenVerify, controller.getAllZonesPolygons);
router.post('/addZone', adminTokenVerify, validateBody(validation.addZone), controller.addZone);
router.post('/getZone', adminTokenVerify, controller.getZone);
router.post('/latInside', adminTokenVerify, controller.latInside);

module.exports = router;
