const express = require('express');
const multer = require('multer');
const upload = multer();
const router = express.Router();

const controller = require('../controllers/orders.controller');
const validation = require('../validations/orders.validation');

const { validateBody } = require('../utils/errors.utils');
const { adminTokenVerify, partnerTokenVerify } = require('../utils/auth.utils');
const { paginationParams } = require('../utils/helper.utils');

router.post('/getAllOrders', adminTokenVerify, paginationParams, validateBody(validation.getAllOrders), controller.getAllOrders);
router.post('/getOrderView', adminTokenVerify, controller.getOrderView);
router.post('/getEagleView', adminTokenVerify, controller.getEagleView);

// MANUAL ASSIGN FROM ADMIN
router.post('/manualAssignPartner', adminTokenVerify, validateBody(validation.manualAssignPartner), controller.manualAssignPartner);
router.post('/getAllAssignablePartners', adminTokenVerify, validateBody(validation.getAllAssignablePartners), controller.getAllAssignablePartners);

// ORDER STATUS CHANGE
router.post('/orderStatusChangeFromAdmin', adminTokenVerify, validateBody(validation.orderStatusChangeFromAdmin), controller.orderStatusChangeFromAdmin);

// PARTNER
router.post('/partnerManualOrder', partnerTokenVerify, validateBody(validation.partnerManualOrder), controller.partnerManualOrder);

module.exports = router;
