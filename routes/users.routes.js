const express = require('express');
const router = express.Router();

const multer = require('multer');
const upload = multer();

const controller = require('../controllers/users.controller');
const { adminTokenVerify, userTokenVerify } = require('../utils/auth.utils');
const { validateBody } = require('../utils/errors.utils');
const validations = require('../validations/users.validation');
const { paginationParams } = require('../utils/helper.utils');

// --------- ADMIN PANEL -------------------
router.post('/getAllUsers', adminTokenVerify, paginationParams, validateBody(validations.getAllUsers), controller.getAllUsers);
router.post('/getEditUser', adminTokenVerify, validateBody(validations.getEditUser), controller.getEditUser);
router.post('/getViewUser', adminTokenVerify, validateBody(validations.getEditUser), controller.getViewUser);
router.post('/changeUserStatus', adminTokenVerify, validateBody(validations.changeUserStatus), controller.changeUserStatus);

// Forgot Password
router.post('/forgotPasswordEmailGetDetails', validateBody(validations.forgotPasswordEmailGetDetails), controller.forgotPasswordEmailGetDetails);
router.post('/forgotPasswordEmailSubmit', validateBody(validations.forgotPasswordEmailSubmit), controller.forgotPasswordEmailSubmit);

// --------- MOBILE ------------------------

// User Registration
router.post('/register/checkEmail', validateBody(validations.registerCheckEmail), controller.registerCheckEmail);
router.post('/register/phoneNumberSendOTP', validateBody(validations.registerPhoneNumberSendOTP), controller.registerPhoneNumberSendOTP);
router.post('/register/verifyOTP', validateBody(validations.registerVerifyOTP), controller.registerVerifyOTP);
router.post('/register/registerNewUser', validateBody(validations.registerNewUser), controller.registerNewUser);
router.post('/register/emailPasswordLogin', validateBody(validations.emailPasswordLogin), controller.emailPasswordLogin);

// Forgot Password
router.post('/forgotPasswordEmail', validateBody(validations.forgotPasswordEmail), controller.forgotPasswordEmail);
router.post('/getDetailsFromToken', validateBody(validations.getDetailsFromToken), controller.getDetailsFromToken);

// Profile
router.post('/checkUserLogin', userTokenVerify, controller.checkUserLogin);
router.post('/updateProfile', userTokenVerify, upload.single('avatar'), validateBody(validations.updateProfile), controller.updateProfile);
router.post('/getProfile', userTokenVerify, validateBody(validations.getProfile), controller.getProfile);
router.post('/changePassword', userTokenVerify, validateBody(validations.changePassword), controller.changePassword);
router.post('/updateLanguage', userTokenVerify, validateBody(validations.updateLanguage), controller.updateLanguage);

router.post('/getNearByPartners', userTokenVerify, paginationParams, validateBody(validations.getNearByPartners), controller.getNearByPartners);

router.post('/emailVerify', userTokenVerify, controller.emailVerify);
router.get('/verifyToken/:token', controller.verifyToken);

// Favorites
router.post('/toggleFavorites', userTokenVerify, validateBody(validations.toggleFavorites), controller.toggleFavorites);
router.post('/getFavorites', userTokenVerify, controller.getFavorites);

// Orders Process
router.post('/getCheckOutAmount', validateBody(validations.getCheckOutAmount), controller.getCheckOutAmount);
router.post('/placeOrder', userTokenVerify, validateBody(validations.placeOrder), controller.placeOrder);
router.post('/retryRequestOrder', userTokenVerify, validateBody(validations.retryRequestOrder), controller.retryRequestOrder);
router.post('/expireOrder', userTokenVerify, validateBody(validations.denyOrder), controller.expireOrder);
router.post('/denyOrder', userTokenVerify, validateBody(validations.denyOrder), controller.denyOrder);
router.post('/cancelOrder', userTokenVerify, validateBody(validations.denyOrder), controller.cancelOrder);

router.post('/trackPartnerLocation', userTokenVerify, validateBody(validations.trackPartnerLocation), controller.trackPartnerLocation);

// Orders Details
router.post('/getOrderDetail', userTokenVerify, validateBody(validations.getOrderDetail), controller.getOrderDetail);
router.post('/getOrdersList', userTokenVerify, paginationParams, validateBody(validations.getOrdersList), controller.getOrdersList);

module.exports = router;
