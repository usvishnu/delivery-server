const schedule = require('node-schedule');
const constantUtils = require('../utils/constant.utils.js');
const Other = require('../models/other.model');
const logs = require('../controllers/logs.controller.js');
const activities = require('../controllers/activities.controller');
const orders = require('../controllers/orders.controller');
const partner = require('../controllers/partner.controller');

const helperUtils = require('../utils/helper.utils');

const startJobs = async (app) => {
    console.log('CRON JOBS Activated');

    // Every Minute
    schedule.scheduleJob(constantUtils.CRON_EVERY_MINUTE, () => {
        partner.cronPartnerOnlineOfflineStatus();
        orders.orderExpireCron();
        helperUtils.cronLogging('CRON_EVERY_MINUTE', [
            'cronPartnerOnlineOfflineStatus',
            'orderExpireCron',
        ]);
    });

    // Every Hour
    schedule.scheduleJob(constantUtils.CRON_EVERY_HOUR, () => {
        helperUtils.updateCityRedis();
        helperUtils.updateLanguageRedis();
        helperUtils.updateGeneralSettingsRedis();
        helperUtils.cronLogging('CRON_EVERY_HOUR', [
            'updateCityRedis',
            'updateLanguageRedis',
            'updateGeneralSettingsRedis',
        ]);
    });

    // Every Day At Midnight
    const config = await Other.config();
    const rule = new schedule.RecurrenceRule();
    rule.hour = 0;
    rule.minute = 0;
    rule.tz = config?.timeZone ?? 'Asia/Kolkata';
    schedule.scheduleJob(rule, () => {
        logs.maintainLogsLimit();
        activities.maintainActivitiesLimit();
        helperUtils.cronLogging('CRON_EVERY_DAY', [
            'maintainLogsLimit',
            'maintainActivitiesLimit',
        ]);
    });
};

module.exports = startJobs;
