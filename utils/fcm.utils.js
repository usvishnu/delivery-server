const FCM = require('fcm-node');
const mongoose = require('mongoose');

const constantUtils = require('../utils/constant.utils');
const Other = require('../models/other.model');
const Log = require('../models/log.model');

const utils = {};

utils.fcmLogging = (message, response, fcmType) => {
    if (mongoose.connection.readyState === 1) {
        Log.create({
            logType: 'FCM',
            data: {
                fcmType,
                fcmMessage: message,
                fcmResponse: response,
            },
        });
    }
};

utils.send = async ({
    userType = '',
    platform = '',
    to = [],
    title = '',
    body = '',
    image = null,
    data = {},
}) => {
    const config = await Other.config();
    let serverKey = '';
    const userAgent =
        platform?.split('/')?.[0]?.toUpperCase() ?? constantUtils.ANDROID;

    // Temporary (Driver for Partner)
    if (userType === constantUtils.PARTNER) {
        if (userAgent === constantUtils.ANDROID)
            serverKey = config?.firebaseAdmin?.androidDriver;
        else serverKey = config?.firebaseAdmin?.iosDriver;
    } else if (userType === constantUtils.USER) {
        if (userAgent === constantUtils.ANDROID)
            serverKey = config?.firebaseAdmin?.androidUser;
        else serverKey = config?.firebaseAdmin?.iosUser;
    }

    const fcm = new FCM(serverKey);

    const message = {
        to,
        priority: 'high',
        content_available: true,
        notification: {
            title: title,
            body: body,
        },
        image: image ?? undefined,
        data,
        sound: 'default',
    };

    fcm.send(message, function (err, response) {
        if (err) {
            try {
                utils.fcmLogging(message, JSON.parse(err), 'FAILURE');
            } catch (err) {
                utils.fcmLogging(message, err, 'FAILURE');
            }
            console.log('Something has gone wrong!');
            console.log(err);
        } else {
            utils.fcmLogging(message, JSON.parse(response), 'SUCCESS');
            console.log('Successfully sent with response: ', response);
        }
    });
};

module.exports = utils;
