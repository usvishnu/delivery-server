const chalk = require('chalk');

const faker = require('faker');
const Admin = require('../models/admin.models');
const Product = require('../models/product.model');
const Other = require('../models/other.model.js');
const Category = require('../models/category.model');
const sms = require('../utils/sms.utils');

const utils = {};

utils.generateRandomOperators = () => {
    const gender = ['MALE', 'FEMALE'];
    const phoneCode = ['+91', '+251'];
    const langCode = ['eng', 'ta'];
    const status = ['ACTIVE', 'INACTIVE', 'ARCHIEVE'];
    const empty_Array = [...new Array(40000)];
    empty_Array.map((each) => {
        Admin.create({
            name: 'OPERATORS',
            data: {
                firstName: faker.name.firstName(),
                lastName: faker.name.lastName(),
                email: faker.internet.email(),
                gender: gender[Math.floor(Math.random() * gender.length)],
                phone: {
                    code: phoneCode[
                        Math.floor(Math.random() * phoneCode.length)
                    ],
                    number: faker.phone.phoneNumber(),
                },
                extraPrivileges: {},
                status: status[Math.floor(Math.random() * status.length)],
                avatar: faker.image.avatar(),
                accessToken: '',
                languageCode:
                    langCode[Math.floor(Math.random() * langCode.length)],
            },
        });
    });
};

utils.generateRandomProducts = () => {
    const price = [100, 300, 200, 80, 20];
    const names = [
        'Tandoori Chicken',
        'Paneer Tikka',
        'Sea Food Starters',
        'Fish 65',
        'Fish Chilli',
        'Fresh Juice',
    ];
    const image = [
        'ttps://b.zmtcdn.com/data/dish_photos/498/0e9cad4ced5becd00abaef1d63a6b498.jpg',
        'https://b.zmtcdn.com/data/dish_photos/498/0e9cad4ced5becd00abaef1d63a6b498.jpg',
        'https://b.zmtcdn.com/data/dish_photos/ae7/d80e1ede2efbecd91057b8712e80eae7.jpg',
        'https://b.zmtcdn.com/data/dish_photos/a77/b1f57855beee4764e70f3671e661da77.jpg',
        'https://b.zmtcdn.com/data/dish_photos/ce7/c20ee6c2c9206119cb7bf2dbfb98cce7.jpg',
        'https://b.zmtcdn.com/data/dish_photos/de8/2f4adffba332f951664e29c75af45de8.jpg',
        'https://b.zmtcdn.com/data/dish_images/1d26c56c308a7291ecea7dd08fadeabf1604139827.jpeg',
    ];
    const status = ['ACTIVE', 'INACTIVE'];
    const vegNon = ['VEG', 'NON-VEG'];
    const empty_Array = [...new Array(0)];
    empty_Array.map((each) => {
        Product.create({
            catalogueId: '61a5ec8a5925c4bfbb134113',
            categoryId: '61a5ed6bbb0187c0a9faf46b',
            subCategoryId: '61a5f237e7397bcab63b724e',
            status: status[Math.floor(Math.random() * status.length)],
            name: names[Math.floor(Math.random() * names.length)],
            image: image[Math.floor(Math.random() * image.length)],
            price: price[Math.floor(Math.random() * price.length)],
            description: faker.lorem.lines(),
            vegNonVeg: vegNon[Math.floor(Math.random() * vegNon.length)],
        }).then((doc) => {
            Category.findByIdAndUpdate(
                '61a5f237e7397bcab63b724e',
                {
                    $push: {
                        products: doc._id,
                    },
                },
                { new: true }
            )
                .then((content) => console.log(content))
                .catch((err) => console.log(err));
            console.log(doc);
        });
    });
};

utils.checkDB = async () => {
    const general = await Other.config();
    if (!general) console.log(chalk.bgRed('General Settings Not Found'));
    if (!(general && general && general.spaces))
        console.log(chalk.bgRed('Spaces key missing in General Settings'));
    // utils.generateRandomProducts();
    // sms.send({
    //     to: '+919791432121',
    //     body: 'Message body',
    //     from: '+12183775925',
    // });
};

utils.isMongoID = (value) => value.match(/^[0-9a-fA-F]{24}$/);

module.exports = utils;
