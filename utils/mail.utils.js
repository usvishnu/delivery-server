const nodemailer = require('nodemailer');

const Other = require('../models/other.model');
const constantUtils = require('../utils/constant.utils');

const utils = {};

utils.sendMail = async (
    to = 'benzigar619@gmail.com',
    subject = 'Initial',
    content = 'Hello'
) => {
    // let testAccount = await nodemailer.createTestAccount();
    const config = await Other.config();
    if (config?.email?.type === constantUtils.GMAIL) {
        let transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: config.gmailConf.email, // generated ethereal user
                pass: config.gmailConf.password, // generated ethereal password
            },
        });
        let info = await transporter.sendMail({
            from: config?.email?.from || '', // sender address
            to, // list of receivers
            subject, // Subject line
            text: 'Hello world?', // plain text body
            html: content, // html body
        });
        console.log('Message sent: %s', info.messageId);
    }
};

module.exports = utils;
