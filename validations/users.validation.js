const Joi = require('joi');
const JoiOid = require('joi-oid');

const constantUtils = require('../utils/constant.utils');
const validationUtils = require('../utils/validation.utils');
const validation = {};

// Router Validations

validation.getAllUsers = Joi.object().keys({
    search: Joi.string().allow(''),
    filters: Joi.object({
        status: Joi.string()
            .valid(
                '',
                constantUtils.ACTIVE,
                constantUtils.INACTIVE,
                constantUtils.ARCHIEVE
            )
            .default(''),
        gender: Joi.string()
            .valid('', constantUtils.MALE, constantUtils.FEMALE)
            .default(''),
        startDate: Joi.string().allow(''),
        endDate: Joi.string().allow(''),
        phoneNumberVerifiedUsers: Joi.string()
            .valid('', 'VERIFIED', 'UNVERIFIED')
            .allow(''),
    }).default({}),
});

validation.getEditUser = Joi.object().keys({
    id: validationUtils.idValidation,
});

validation.retryRequestOrder = Joi.object().keys({
    orderId: validationUtils.idValidation.required(),
});

validation.cancelOrder = Joi.object().keys({
    orderId: validationUtils.idValidation.required(),
});

validation.registerCheckEmail = Joi.object().keys({
    email: validationUtils.emailValidation,
});

validation.registerPhoneNumberSendOTP = Joi.object().keys({
    phoneCode: validationUtils.phoneCodeValidation,
    phoneNumber: validationUtils.phoneNumberValidation,
});

validation.getCheckOutAmount = Joi.object().keys({
    cityId: validationUtils.idValidation.required(),
    shopId: validationUtils.idValidation.required(),
    products: Joi.array()
        .items(
            Joi.object().keys({
                id: validationUtils.idValidation.required(),
                quantity: Joi.number().min(1).required(),
            })
        )
        .required(),
});

validation.placeOrder = Joi.object().keys({
    shopId: validationUtils.idValidation.required(),
    paymentType: validationUtils.paymentValidation,
    products: Joi.array()
        .items(
            Joi.object().keys({
                id: validationUtils.idValidation.required(),
                quantity: Joi.number().min(1),
            })
        )
        .min(1)
        .required(),
    deliveryAddress: Joi.object()
        .keys({
            addressHouseNo: Joi.string().allow(''),
            addressType: Joi.string().allow(''),
            addressDirection: Joi.string().allow(''),
            shortAddress: Joi.string().allow(''),
            fullAddress: Joi.string().required(),
            lat: validationUtils.latValidation,
            lng: validationUtils.lngValidation,
        })
        .required(),
});

validation.denyOrder = Joi.object().keys({
    orderId: validationUtils.idValidation.required(),
});

validation.getOrderDetail = Joi.object().keys({
    orderId: validationUtils.idValidation.required(),
});

validation.trackPartnerLocation = Joi.object().keys({
    partnerId: validationUtils.idValidation.required(),
});

validation.getOrdersList = Joi.object().keys({
    orderType: Joi.string().valid('ALL', 'COMPLETED', 'ONGOING').required(),
});

validation.registerVerifyOTP = Joi.object().keys({
    phoneCode: validationUtils.phoneCodeValidation,
    phoneNumber: validationUtils.phoneNumberValidation,
    otp: validationUtils.otpValidation,
});

validation.registerNewUser = Joi.object().keys({
    firstName: validationUtils.nameValidation,
    lastName: validationUtils.nameValidation,
    email: validationUtils.emailValidation,
    password: validationUtils.passwordValidation,
    phoneCode: validationUtils.phoneCodeValidation,
    phoneNumber: validationUtils.phoneNumberValidation,
    languageCode: Joi.string().required(),
    deviceId: Joi.string().required(),
    platform: validationUtils.platformValidation,
    otp: validationUtils.otpValidation,
});

validation.emailPasswordLogin = Joi.object().keys({
    email: validationUtils.emailValidation,
    password: validationUtils.passwordValidation,
    deviceId: Joi.string().required(),
    platform: validationUtils.platformValidation,
});

validation.forgotPasswordEmail = Joi.object().keys({
    email: validationUtils.emailValidation,
});

validation.getNearByPartners = Joi.object().keys({
    lat: validationUtils.lngValidation,
    lng: validationUtils.lngValidation,
    distance: Joi.number().min(0).max(100000).required(),
});

validation.forgotPasswordEmailGetDetails = Joi.object().keys({
    token: Joi.string().required(),
});

validation.forgotPasswordEmailSubmit = Joi.object().keys({
    token: Joi.string().required(),
    password: validationUtils.passwordValidation,
});

validation.getProfile = Joi.object().keys({
    deviceId: Joi.string().required(),
    platform: validationUtils.platformValidation,
});

validation.getDetailsFromToken = Joi.object().keys({
    accessToken: Joi.string().required(),
});

validation.changeUserStatus = Joi.object().keys({
    id: Joi.array().items(Joi.string().required()).required(),
    status: validationUtils.statusValidation,
});

validation.updateProfile = Joi.object().keys({
    email: validationUtils.emailValidation,
    firstName: validationUtils.nameValidation,
    lastName: validationUtils.nameValidation,
});

validation.changePassword = Joi.object().keys({
    oldPassword: validationUtils.passwordValidation,
    newPassword: validationUtils.passwordValidation,
});

validation.updateLanguage = Joi.object().keys({
    languageCode: Joi.string().required(),
});

validation.toggleFavorites = Joi.object().keys({
    id: validationUtils.idValidation.required(),
});

module.exports = validation;
