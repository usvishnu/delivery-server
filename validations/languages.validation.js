const Joi = require('joi');

const validationUtils = require('../utils/validation.utils');

const validation = {};

// Router Validations
validation.addLanguage = Joi.object().keys({
    id: validationUtils.idValidation,
    languageCode: Joi.string().required(),
    languageName: validationUtils.nameValidation,
    languageNativeName: Joi.string().required(),
    languageMobileUserKeys: Joi.array().required(),
    languageMobilePartnerKeys: Joi.array().required(),
    languageAdminKeys: Joi.object().required(),
    languageDirection: Joi.string().valid('LTR', 'RTL').required(),
    languageDefault: Joi.boolean().required(),
    autoGenerateKeys: Joi.boolean(),
});

validation.setDefaultLanguage = Joi.object().keys({
    id: validationUtils.idValidation,
});

validation.getLanguage = Joi.object().keys({
    id: validationUtils.idValidation,
});

validation.getViewLanguage = Joi.object().keys({
    id: validationUtils.idValidation,
});

validation.updateMobileKeys = Joi.object().keys({
    langCode: Joi.string().required().min(1).max(10),
    userType: Joi.string().valid('USER', 'PARTNER').required(),
    type: Joi.string()
        .valid('FLUSH', 'PUSH', 'REPLACE', 'REPLACE_SINGLE')
        .required()
        .default('REPLACE'),
    langKey: Joi.string(),
    langKeys: Joi.array().items(
        Joi.object().keys({
            key: Joi.string().min(1).required(),
            value: Joi.string().min(1).required(),
        })
    ),
});

validation.bingTranslateAdminKeys = Joi.object().keys({
    id: validationUtils.idValidation,
    langKeys: Joi.object().required(),
});

module.exports = validation;
