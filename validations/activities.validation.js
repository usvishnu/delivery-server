const Joi = require('joi');
const JoiOid = require('joi-oid');

const constantUtils = require('../utils/constant.utils');
const validationUtils = require('../utils/validation.utils');
const validation = {};

validation.allActivities = Joi.object().keys({});

module.exports = validation;
