const Joi = require('joi');
const JoiOid = require('joi-oid');

const constantUtils = require('../utils/constant.utils');
const validationUtils = require('../utils/validation.utils');
const validation = {};

// Router Validations

validation.updateGeneralSettings = Joi.object().keys({
    deepLinkBaseUser: Joi.string().required(),
    deepLinkBasePartner: Joi.string().required(),
    pageViewLimits: Joi.number().max(80).required(),
    otpLength: Joi.number().min(3).max(12).required(),
    otpRetrySeconds: Joi.number().min(1).max(500).required(),
    secureKey: Joi.string().required(),
    darkLogo: Joi.string().required(),
    defaultDialCode: Joi.string().required(),
    favicon: Joi.string().required(),
    lightLogo: Joi.string().required(),
    emailAddress: Joi.string().email().required(),
    mapApi: Joi.object()
        .keys({
            web: Joi.string().required(),
            android: Joi.string().required(),
            ios: Joi.string().required(),
        })
        .required(),
    shop: Joi.object()
        .keys({
            type: Joi.string().required(),
            id: Joi.string().required(),
            allowDeleteCatalogueContents: Joi.boolean().required(),
        })
        .required(),
    partner: Joi.object()
        .keys({
            showStockPartnerInAllShops: Joi.boolean().required(),
            minimumStock: Joi.number().min(0).required(),
            editableVehicleKeys: Joi.array().required(),
            documentStatus: Joi.boolean().required(),
            minAgeToRegister: Joi.number().min(0).required(),
            onlineStatusThreshold: Joi.number().required(),
        })
        .required(),
    viewShopManagementInDashboard: Joi.boolean().required(),
    liveRefreshSeconds: Joi.number().required(),
    order: Joi.object()
        .keys({
            prefix: Joi.string().required(),
            orderExpireThreshold: Joi.number().min(0).required(),
        })
        .required(),
    adminPanelBaseUrl: Joi.string().required(),
    otpMode: Joi.string().valid('DEVELOPMENT', 'PRODUCTION').required(),
    apiBaseUrl: Joi.string().required(),
    email: Joi.object()
        .keys({
            type: Joi.string().allow('GMAIL').required(),
            from: Joi.string().required(),
        })
        .required(),
    gmailConf: Joi.object()
        .keys({
            email: Joi.string().email().required(),
            password: Joi.string().required(),
        })
        .required(),
    spaces: Joi.object()
        .keys({
            spacesKey: Joi.string().required(),
            spacesSecret: Joi.string().required(),
            spacesEndpoint: Joi.string().required(),
            spacesBucketName: Joi.string().required(),
            spacesBaseUrl: Joi.string().required(),
            spacesObjectName: Joi.string().required(),
        })
        .required(),
    firebaseAdmin: Joi.object()
        .keys({
            androidUser: Joi.string().required(),
            iosUser: Joi.string().required(),
            androidDriver: Joi.string().required(),
            iosDriver: Joi.string().required(),
        })
        .required(),
    scrapping: Joi.object()
        .keys({
            bingTranslateSelector: Joi.string().required(),
            translateConcurrentJobs: Joi.number().min(0).max(40).required(),
        })
        .required(),
    mapApiArray: Joi.object()
        .keys({
            web: Joi.array().required(),
            android: Joi.array().required(),
            ios: Joi.array().required(),
        })
        .required(),
    maintainDocs: Joi.object()
        .keys({
            activitiesDocsLimit: Joi.number().required(),
            logsDocsLimit: Joi.number().required(),
        })
        .required(),
    sendError: Joi.object()
        .keys({
            status: Joi.boolean().required(),
            email: Joi.string().email().required(),
        })
        .required(),
    timeZone: Joi.string().required(),
    partnerLocationUpdateInterval: Joi.number().required(),
    partnerOnOrderLocationUpdateInterval: Joi.number().required(),
});

validation.updatePassword = Joi.object().keys({
    oldPassword: validationUtils.passwordValidation,
    newPassword: validationUtils.passwordValidation,
});

validation.forgotPasswordWithEmail = Joi.object().keys({
    email: validationUtils.emailValidation,
});

validation.verifyOTPWithEmail = Joi.object().keys({
    email: validationUtils.emailValidation,
    otp: validationUtils.otpValidation,
});

validation.resetPasswordWithEmail = Joi.object().keys({
    email: validationUtils.emailValidation,
    newPassword: validationUtils.passwordValidation,
    otp: validationUtils.otpValidation,
});

validation.loginWithPhone = Joi.object().keys({
    phoneCode: validationUtils.phoneCodeValidation,
    phoneNumber: validationUtils.phoneNumberValidation,
    password: validationUtils.passwordValidation,
});

validation.loginWithEmail = Joi.object().keys({
    email: validationUtils.emailValidation,
    password: validationUtils.passwordValidation,
});

validation.forgotPassword = Joi.object().keys({
    email: validationUtils.emailValidation,
});

validation.getOperator = Joi.object().keys({
    id: validationUtils.idValidation,
});

validation.changePreferredTheme = Joi.object().keys({
    theme: Joi.string().required().min(1).max(20),
});

validation.updateProfileLanguage = Joi.object().keys({
    languageCode: Joi.string().required(),
});

validation.addOperator = Joi.object().keys({
    id: validationUtils.idValidation,
    firstName: validationUtils.nameValidation,
    lastName: validationUtils.nameValidation,
    email: validationUtils.emailValidation,
    gender: validationUtils.genderValidation,
    avatar: validationUtils.urlValidation,
    phoneCode: validationUtils.phoneCodeValidation,
    phoneNumber: validationUtils.phoneNumberValidation,
    password: validationUtils.optionalPasswordValidation,
    languageCode: Joi.string().required().min(2).max(6),
    status: validationUtils.statusValidation,
});

validation.getAllOperators = Joi.object().keys({
    search: Joi.string().allow(''),
    filters: Joi.object({
        userType: Joi.string(),
        status: Joi.string()
            .valid(
                '',
                constantUtils.ACTIVE,
                constantUtils.INACTIVE,
                constantUtils.ARCHIEVE
            )
            .default(''),
        gender: Joi.string()
            .valid('', constantUtils.MALE, constantUtils.FEMALE)
            .default(''),
        phoneCode: Joi.string().default('').allow(''),
        sort: Joi.string().valid('', 'LAST_LOGIN').default(''),
        startDate: Joi.string().allow(''),
        endDate: Joi.string().allow(''),
        login: Joi.string()
            .allow('', constantUtils.LOGGED, constantUtils.NOT_LOGGED)
            .default(''),
    }).default({}),
});

validation.statusChangeOperator = Joi.object().keys({
    id: validationUtils.idValidation,
    status: Joi.string()
        .allow(
            constantUtils.ACTIVE,
            constantUtils.INACTIVE,
            constantUtils.ARCHIEVE
        )
        .required(),
});

validation.editGetOperator = Joi.object().keys({
    id: validationUtils.idValidation,
});

validation.updateProfile = Joi.object().keys({
    firstName: validationUtils.nameValidation,
    lastName: validationUtils.nameValidation,
    email: validationUtils.emailValidation,
    gender: validationUtils.genderValidation,
    phoneCode: validationUtils.phoneCodeValidation,
    phoneNumber: validationUtils.phoneNumberValidation,
    languageCode: Joi.string().required().min(2).max(6),
    preferredTheme: Joi.string().required(),
});

validation.checkError = Joi.object().keys({
    type: Joi.string().required(),
});

validation.socketEmit = Joi.object().keys({
    namespace: Joi.string().required(),
    socketId: Joi.string(),
    socketEvent: Joi.string().required(),
    data: Joi.object().required(),
});

validation.restartApp = Joi.object().keys({
    platform: Joi.string().allow('WEB', 'MOBILE').required(),
    socketId: Joi.string(),
});

module.exports = validation;
