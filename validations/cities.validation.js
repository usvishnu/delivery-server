const Joi = require('joi');

const constantUtils = require('../utils/constant.utils');
const validationUtils = require('../utils/validation.utils');

module.exports = {
    addCity: Joi.object({
        id: validationUtils.idValidation,
        locationName: validationUtils.nameValidation,
        currencySymbol: Joi.string().required(),
        currencyCode: Joi.string().required(),
        timezone: Joi.string().required(),
        location: Joi.array()
            .items(Joi.array().items(Joi.array().items(Joi.number())))
            .required(),
        algorithm: Joi.object({
            partnerType: Joi.string().required(),
            notificationType: Joi.string().required(),
            payoutMethod: Joi.string().required(),
            billingDaysInterval: Joi.number().min(0).required(),
            acceptPaymentModes: Joi.array().items(Joi.string()).required(),
            region: Joi.object({
                zoneOrNearBy: Joi.string().required(),
                nearByRadius: Joi.number().required(),
            }).required(),
            debitTo: Joi.array().items(Joi.string()).required(),
            maxOrdersAtTime: Joi.number().min(1).required(),
            orderRequestTimeOut: Joi.number().min(0).required(),
            orderRetryCount: Joi.number().min(0).required(),
            arriveRadius: Joi.number().min(0).required(),
        }).required(),
        orderCalculation: Joi.object({
            serviceTaxPercentage: validationUtils.percentageValidation,
            partner: Joi.object({
                minimumDeliveryCharge: Joi.number().min(0).required(),
                farePerDistance: Joi.number().min(0).required(),
                siteCommissionPercentage: validationUtils.percentageValidation,
                nightFare: Joi.object({
                    status: Joi.boolean().required(),
                    fareRatio: Joi.number().required(),
                    startTime: Joi.date().required(),
                    endTime: Joi.date().required(),
                    siteCommissionPercentage:
                        validationUtils.percentageValidation,
                }),
                cancellation: validationUtils.cancellationValidation,
            }).required(),
            shop: Joi.object({
                siteCommissionPercentage: validationUtils.percentageValidation,
                packagePerProduct: Joi.number().required(),
            }).required(),
            user: Joi.object({
                cancellation: validationUtils.cancellationValidation,
            }),
        }).required(),
    }),
    addZone: Joi.object({
        id: validationUtils.idValidation,
        cityId: validationUtils.idValidation,
        zoneName: validationUtils.nameValidation,
        location: Joi.array()
            .items(Joi.array().items(Joi.array().items(Joi.number())))
            .required(),
    }),
};
