const partnerController = require('../controllers/partner.controller');
const partnerValidation = require('../validations/partners.validation');
const { validateSocket } = require('../utils/errors.utils');

const constantUtils = require('../utils/constant.utils');

const partnerSocket = (io, socket, partner) => {
    socket.on(constantUtils?.SOCKET_EVENTS?.UPDATE_LOCATION, (body) => {
        if (
            validateSocket(
                partnerValidation.updateLocation,
                constantUtils?.NS?.PARTNERS,
                constantUtils?.SOCKET_EVENTS?.UPDATE_LOCATION,
                body,
                socket,
                partner
            )
        )
            partnerController.partnerLocationUpdateSocket(
                io,
                socket,
                partner,
                body
            );
    });
};

module.exports = partnerSocket;
