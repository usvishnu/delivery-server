/* NPM DEPENDENCIES */
const mongoose = require('mongoose');

const constantUtils = require('../utils/constant.utils');

const ratingSchema = new mongoose.Schema(
    {
        ratingFor: {
            type: String,
            enum: [
                constantUtils.USER,
                constantUtils.PARTNER,
                constantUtils.PRODUCT,
                constantUtils.ORDER,
            ],
            index: true,
        },
        userId: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'users',
        },
        productId: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'partners',
            index: true,
        },
        partnerId: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'partners',
            index: true,
        },
        orderId: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'orders',
        },
        message: {
            type: String,
        },
        rating: {
            type: Number,
            required: true,
            index: true,
        },
        images: [String],
    },
    { timestamps: true, versionKey: false }
);

const Rating = mongoose.model('ratings', ratingSchema, 'dratings');

module.exports = Rating;
