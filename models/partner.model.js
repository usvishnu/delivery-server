/* NPM DEPENDENCIES */
const mongoose = require('mongoose');

const constantUtils = require('../utils/constant.utils');

const { AvailableProductSchema } = require('./shop.model');
const { RatingSchema } = require('./product.model');

const PointSchema = new mongoose.Schema({
    type: {
        type: String,
        enum: ['Point'],
    },
    coordinates: {
        type: [Number],
    },
});

const DocumentSchema = new mongoose.Schema(
    {
        documentName: {
            type: String,
        },
        documents: [
            {
                type: String,
            },
        ],
        expiryDate: {
            type: Date,
        },
        status: {
            type: String,
            default: constantUtils.ACTIVE,
        },
    },
    {
        _id: false,
    }
);

const onGoingOrderSchema = new mongoose.Schema(
    {
        orderId: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'orders',
        },
        acceptedTime: {
            type: Date,
        },
        status: {
            type: String,
        },
    },
    {
        _id: false,
    }
);

const zoneSchema = new mongoose.Schema(
    {
        zoneId: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            index: true,
            ref: 'zones',
        },
        zoneType: {
            type: String,
            default: 'PRIMARY',
            required: true,
        },
    },
    {
        _id: false,
    }
);

const partnerSchema = new mongoose.Schema(
    {
        cityId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'cities',
            index: true,
            required: true,
        },
        zones: [zoneSchema],
        firstName: {
            type: String,
            trim: true,
            default: '',
        },
        lastName: {
            type: String,
            trim: true,
            default: '',
        },
        email: {
            type: String,
            trim: true,
            lowercase: true,
            index: {
                unique: true,
                partialFilterExpression: { email: { $type: 'string' } },
            },
        },
        dob: {
            type: Date,
            required: true,
        },
        partnerType: {
            type: String,
            enum: [
                constantUtils.STOCK_PARTNER,
                constantUtils.INDEPENDENT_PARTNER,
                constantUtils.SHOP_PARTNER,
            ],
            required: true,
            index: true,
        },
        partnerDocuments: [DocumentSchema],
        catalogueId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'catalogues',
        },
        availableProducts: [AvailableProductSchema],
        gender: {
            type: String,
            trim: true,
            default: '',
        },
        phone: {
            code: {
                type: String,
                required: true,
                trim: true,
            },
            number: {
                type: String,
                unique: true,
                required: true,
                trim: true,
            },
        },
        otp: {
            type: String,
            trim: true,
        },
        phoneNumberOtp: {
            type: String,
            trim: true,
        },
        phoneNumberVerifiedTime: {
            type: Date,
        },
        avatar: {
            type: String,
            default: constantUtils.DEFAULTAVATAR,
            trim: true,
        },
        languageCode: {
            type: String,
            default: 'en',
            trim: true,
        },
        emailToken: {
            type: String,
        },
        emailVerificationTime: {
            type: Date,
        },
        vehicles: [
            {
                vehicleId: {
                    type: mongoose.Schema.Types.ObjectId,
                    optional: true,
                    ref: 'vehicles',
                },
                defaultVehicle: {
                    required: true,
                    type: Boolean,
                    default: true,
                },
            },
        ],
        onlineStatus: { type: Boolean, default: false, index: true },
        lastOnlineStatusChange: {
            type: Date,
        },
        currencyCode: { type: String, default: 'USD' },
        status: {
            type: String,
            required: true,
            trim: true,
            enum: [constantUtils.ACTIVE, constantUtils.INACTIVE],
            default: constantUtils.ACTIVE,
        },
        statusLastEdited: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'admins',
        },
        reFillRequest: {
            status: {
                type: String,
                enum: [
                    constantUtils.NEW,
                    constantUtils.ATTENDED,
                    constantUtils.CLOSED,
                ],
                optional: true,
            },
            attendedBy: {
                type: mongoose.Schema.Types.ObjectId,
                optional: true,
                ref: 'admins',
            },
            newTime: {
                type: Date,
                optional: true,
            },
            attendedTime: {
                type: Date,
                optional: true,
            },
            closedTime: {
                type: Date,
                optional: true,
            },
        },
        deviceInfo: [
            {
                deviceId: {
                    type: String,
                    trim: true,
                    default: '',
                },
                deviceId: {
                    type: String,
                    trim: true,
                    default: '',
                },
                deliverySocketId: {
                    type: String,
                    trim: true,
                    default: '',
                },
                socketId: {
                    type: String,
                    trim: true,
                    default: '',
                },
                deviceType: {
                    /* WEB, ANDROID, IOS */
                    type: String,
                    trim: true,
                },
                accessToken: {
                    type: String,
                    trim: true,
                    index: true,
                },
                platform: {
                    type: String,
                    trim: true,
                },
                createdAt: {
                    type: 'Date',
                    default: Date.now(),
                },
            },
        ],
        location: {
            _id: 0,
            type: PointSchema,
            index: '2dsphere',
            default: {
                type: 'Point',
                coordinates: [0, 0],
            },
        },
        lastEdited: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'admins',
        },
        addedBy: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'admins',
        },
        password: {
            type: String,
            required: true,
        },
        lastEditedTime: {
            type: Date,
            optional: true,
        },
        currentBearing: { type: Number },
        speed: { type: Number },
        isLocationUpdated: { type: Boolean, default: false },
        locationUpdatedTime: { type: Date },
        onGoingOrders: [onGoingOrderSchema],
        rating: RatingSchema,
    },
    { timestamps: true, versionKey: false }
);

const partner = mongoose.model('partners', partnerSchema, 'dpartners');

partner.DocumentSchema = DocumentSchema;

module.exports = partner;
