/* NPM DEPENDENCIES */
const mongoose = require('mongoose');

const constantUtil = require('../utils/constant.utils');
const { DocumentSchema } = require('./partner.model');

const vehicleSchema = new mongoose.Schema(
    {
        ownerId: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'partners',
        },
        assignedId: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'partners',
        },
        name: {
            type: String,
            lowercase: true,
            trim: true,
        },
        plateNumber: {
            type: String,
        },
        maker: {
            type: String,
        },
        model: {
            type: String,
        },
        year: {
            type: String,
        },
        notes: {
            type: String,
        },
        color: {
            type: String,
        },
        noOfDoors: {
            type: Number,
        },
        noOfSeats: {
            type: Number,
        },
        frontImage: {
            type: String,
        },
        backImage: {
            type: String,
        },
        leftImage: {
            type: String,
        },
        rightImage: {
            type: String,
        },
        status: {
            /* ACTIVE, INACTIVE, ARCHIVE */
            type: String,
            required: true,
            trim: true,
            enum: [constantUtil.ACTIVE, constantUtil.INACTIVE],
            default: constantUtil.ACTIVE,
        },
        vehicleDocuments: [DocumentSchema],
        addedBy: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'admins',
        },
        lastEdited: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'admins',
        },
        lastEditedTime: {
            default: Date.now,
            type: Date,
            required: true,
        },
    },
    { timestamps: true, versionKey: false }
);

const vehicles = mongoose.model('vehicles', vehicleSchema, 'dvehicles');

module.exports = vehicles;
