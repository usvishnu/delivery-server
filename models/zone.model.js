/* NPM DEPENDENCIES */
const mongoose = require('mongoose');

const constantUtils = require('../utils/constant.utils');

const locationSchema = new mongoose.Schema(
    {
        type: {
            type: String,
            enum: ['Polygon'],
            required: true,
        },
        coordinates: {
            type: [[[Number]]],
            required: true,
        },
    },
    {
        _id: false,
    }
);

const zoneSchema = new mongoose.Schema(
    {
        cityId: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'cities',
        },
        zoneName: {
            type: String,
            lowercase: true,
            trim: true,
            required: true,
        },
        status: {
            /* INCOMPLETE, ACTIVE, INACTIVE, ARCHIVE */
            type: String,
            required: true,
            trim: true,
            enum: [
                constantUtils.INCOMPLETE,
                constantUtils.ACTIVE,
                constantUtils.INACTIVE,
            ],
            default: constantUtils.ACTIVE,
        },
        location: {
            type: locationSchema,
        },
        addedBy: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'admins',
        },
        lastEdited: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'admins',
        },
        lastEditedTime: {
            default: Date.now,
            type: Date,
            required: true,
        },
        orderCalculation: {
            peakFare: {
                fareRatio: {
                    type: Number,
                },
            },
            nightFare: {
                fareRatio: {
                    type: Number,
                },
            },
        },
    },
    { timestamps: true, versionKey: false }
);

zoneSchema.index({ location: '2dsphere' });

const Zone = mongoose.model('zones', zoneSchema, 'dzones');

Zone.getZoneFromLatLng = (lat, lng, status = constantUtils.ACTIVE) =>
    new Promise(async (res, rej) => {
        try {
            const zone = await Zone.findOne({
                location: {
                    $geoIntersects: {
                        $geometry: {
                            type: 'Point',
                            coordinates: [lng, lat],
                        },
                    },
                },
                status,
            }).lean();
            res(zone);
        } catch (err) {
            rej(err);
        }
    });

module.exports = Zone;
