const mongoose = require('mongoose');

const redis = require('../utils/redis.utils');
const constantUtils = require('../utils/constant.utils');

const otherSchema = new mongoose.Schema(
    {
        name: String,
        data: Object,
    },
    {
        versionKey: false,
        timestamps: true,
    }
);

const Other = mongoose.model('others', otherSchema, 'dothers');

Other.config = async function () {
    const ifConfig = await redis.get(constantUtils.GENERALSETTING);
    if (ifConfig) return ifConfig.data;
    const config = await Other.findOne({
        name: constantUtils.GENERALSETTING,
    }).lean();
    await redis.set(constantUtils.GENERALSETTING, config);
    return config.data;
};

module.exports = Other;
