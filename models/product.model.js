/* NPM DEPENDENCIES */
const mongoose = require('mongoose');

const constantUtil = require('../utils/constant.utils');

const RatingSchema = new mongoose.Schema({
    noOfOrdersRated: {
        type: Number,
    },
    ratingAverage: {
        type: Number,
    },
    ratingInfo: [
        new mongoose.Schema(
            {
                rating: {
                    type: Number,
                },
                total: {
                    type: Number,
                },
            },
            {
                _id: false,
            }
        ),
    ],
});

const cityIdSchema = new mongoose.Schema(
    {
        cityId: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'cities',
            index: true,
        },
    },
    {
        _id: false,
    }
);

const productSchema = new mongoose.Schema(
    {
        catalogueId: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'catalogues',
        },
        categoryId: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'categories',
        },
        subCategoryId: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'categories',
        },
        cities: [cityIdSchema],
        name: {
            type: String,
            trim: true,
            default: '',
            index: true,
        },
        productType: {
            type: String,
            enum: [constantUtil.PREPARATION, constantUtil.QUANTITY_STOCK],
            required: true,
        },
        image: {
            type: String,
            trim: true,
        },
        secondaryImage: {
            type: String,
            trim: true,
        },
        maxCartLimit: {
            type: Number,
            required: true,
        },
        status: {
            type: String,
            required: true,
            trim: true,
            enum: [constantUtil.ACTIVE, constantUtil.INACTIVE],
            default: constantUtil.ACTIVE,
            index: true,
        },
        price: {
            type: Number,
            required: true,
            default: 0,
        },
        description: {
            type: String,
            default: '',
        },
        vegNonVeg: {
            type: String,
            enum: ['VEG', 'NON-VEG'],
            index: true,
        },
        statusLastEdited: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'admins',
        },
        tags: [
            {
                type: String,
                enum: [constantUtil.BEST_SELLER, constantUtil.MUST_TRY],
            },
        ],
        lastEdited: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'admins',
        },
        addedBy: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'admins',
        },
        lastEditedTime: {
            type: Date,
            optional: true,
        },
        rating: RatingSchema,
    },
    { timestamps: true, versionKey: false }
);

const products = mongoose.model('products', productSchema, 'dproducts');

products.RatingSchema = RatingSchema;

module.exports = products;
