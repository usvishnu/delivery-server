const mongoose = require('mongoose');

const languageKeysSchema = new mongoose.Schema(
    {
        key: {
            type: String,
            required: true,
            unique: true,
        },
        value: {
            type: String,
            required: true,
        },
    },
    {
        _id: false,
    }
);

const languageSchema = new mongoose.Schema(
    {
        languageName: {
            type: String,
            required: true,
        },
        languageNativeName: {
            type: String,
            required: true,
        },
        languageCode: {
            type: String,
            required: true,
            unique: true,
        },
        languageDirection: {
            type: String,
            enum: ['LTR', 'RTL'],
            required: true,
            default: 'LTR',
        },
        languageDefault: {
            type: Boolean,
            default: false,
        },
        languageMobileUserKeys: [languageKeysSchema],
        languageMobilePartnerKeys: [languageKeysSchema],
        languageAdminKeys: {
            type: Object,
            required: true,
        },
        addedBy: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'admins',
        },
        lastEdited: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'admins',
        },
        lastEditedTime: {
            default: Date.now,
            type: Date,
            required: true,
        },
    },
    {
        versionKey: false,
        timestamps: true,
    }
);

const Language = mongoose.model('languages', languageSchema, 'dlanguages');

module.exports = Language;
