const mongoose = require('mongoose');

const constantUtils = require('../utils/constant.utils');

const logSchema = new mongoose.Schema(
    {
        logType: {
            type: String,
            required: true,
            enum: [
                'API',
                'CRON',
                'RESTART_SERVER',
                'ERROR',
                'SOCKET',
                'CONSOLE',
                'VALIDATION',
                'FCM',
                'CLIENT_ERROR',
            ],
            index: true,
        },
        data: {
            // validation
            validationType: String,
            validationMessage: Array,
            // socket
            socketUserType: String,
            socketConnectionType: String,
            socketId: String,
            socketUserData: Object,
            // api
            apiUrl: String,
            apiStatusCode: Number,
            apiPlatform: String,
            apiSource: String,
            time: Number,
            cronType: String,
            process: Array,
            message: String,
            // console
            consoleData: mongoose.Schema.Types.Mixed,
            reqBody: Object,
            // fcm
            fcmType: String,
            fcmMessage: Object,
            fcmResponse: Object,
            errorCode: String,
            userDetail: Object,
        },
    },
    {
        versionKey: false,
        timestamps: true,
    }
);

logSchema.index({
    'data.socketUserType': 1,
    'data.apiUrl': 1,
    'data.cronType': 1,
    'data.socketId': 1,
});

const Log = mongoose.model('logs', logSchema, 'dlogs');

module.exports = Log;
