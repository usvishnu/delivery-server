/* NPM DEPENDENCIES */
const mongoose = require('mongoose');

const constantUtils = require('../utils/constant.utils');

const notificationSchema = new mongoose.Schema(
    {
        userId: {
            type: mongoose.Schema.Types.ObjectId,
            refPath: 'userType',
            index: true,
        },
        userType: {
            type: String,
            required: true,
            enum: [
                constantUtils.NOTIFICATIONS.USERS,
                constantUtils.NOTIFICATIONS.PARTNERS,
                constantUtils.NOTIFICATIONS.ADMINS,
            ],
            index: true,
        },
        notificationType: {
            type: String,
            required: true,
            enum: [constantUtils.NOTIFICATIONS.REQUEST_RE_FILL],
        },
        notificationContent: {
            partnerId: {
                type: mongoose.Schema.Types.ObjectId,
                optional: true,
                ref: 'partners',
            },
        },
        completeStatus: {
            type: Boolean,
        },
        notificationTargetType: {
            type: String,
            required: true,
            enum: [constantUtils.SINGLE, constantUtils.ALL],
            index: true,
        },
    },
    { timestamps: true, versionKey: false }
);

const notification = mongoose.model(
    'notifications',
    notificationSchema,
    'dnotifications'
);

module.exports = notification;
