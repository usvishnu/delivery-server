const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const handler = require('express-async-handler');
const cors = require('cors');
const useragent = require('express-useragent');
// const compression = require('compression');

// process.env.TZ = 'UTC';

const app = express();
const serverStartTime = new Date();

const config = require('./config');

// Utils
const { connectSocket } = require('./utils/socket.utils');
const errorUtil = require('./utils/errors.utils');
const initiateCron = require('./utils/crons.utils');
const dbUtil = require('./utils/db.utils');
const helperUtil = require('./utils/helper.utils');

const checkDB = dbUtil.checkDB;

const http = require('http').Server(app);

// app.disable('etag');
app.use(cors('*'));
app.use(express.json({ limit: '100mb' }));
app.use(express.urlencoded({ limit: '100mb', extended: true }));
app.use(useragent.express());
// app.use(express.static('public'));
// app.use(compression());

// Logging
app.use(
    morgan(':status\t:method\t:response-time ms\t:url', {
        skip: function (req, res) {
            return res.statusCode === 204;
        },
    })
);

app.get('/favicon.ico', function (req, res) {
    res.status(204);
    res.end();
});

app.use(helperUtil.apiLogging);

// Routes
if (
    config.ENVIRONMENT === 'development' ||
    (config.ENVIRONMENT === 'production' && config.SERVICE === 'ALL')
) {
    connectSocket(app, config.SOCKET_PORT);
    app.use('/api/delivery/admins', require('./routes/admins.routes'));
    app.use('/api/delivery/users', require('./routes/users.routes'));
    app.use('/api/delivery/languages', require('./routes/languages.routes'));
    app.use('/api/delivery/screens', require('./routes/screens.routes'));
    app.use('/api/delivery/activities', require('./routes/activities.routes'));
    app.use('/api/delivery/logs', require('./routes/logs.routes'));
    app.use('/api/delivery/cities', require('./routes/cities.routes'));
    app.use('/api/delivery/shops', require('./routes/shops.routes'));
    app.use('/api/delivery/partners', require('./routes/partner.routes'));
    app.use('/api/delivery/orders', require('./routes/orders.routes'));
    app.use('/api/delivery/ratings', require('./routes/ratings.routes'));
}

// Not Found
app.use(
    '*',
    handler((req, res) => {
        throw '404|NOT_FOUND';
    })
);

// Error Handling
app.use((err, req, res, next) => errorUtil.handlerError(err, req, res));

// Staring the server
const start = async () => {
    // Mongo Connect
    await mongoose.connect(config.MONGO, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false,
    });
    console.table(config);
    await checkDB();

    initiateCron(app);
    http.listen(config.PORT, () => {
        console.log(`Server running in ${config.PORT}`);
        helperUtil.restartServerLog(new Date() - serverStartTime);
    });
};

start();
