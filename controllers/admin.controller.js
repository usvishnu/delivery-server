const handler = require('express-async-handler');
const timezones = require('timezones-list');
const sharp = require('sharp');
const parallel = require('async/parallel');
const { exec } = require('child_process');
const { dot } = require('dot-object');
const si = require('systeminformation');
const mongoose = require('mongoose');

const Admin = require('../models/admin.models');
const Other = require('../models/other.model');
const Language = require('../models/language.model');
const Activity = require('../models/activity.model');
const User = require('../models/user.model');
const Notification = require('../models/notification.model');
const Order = require('../models/order.model');

const constantUtils = require('../utils/constant.utils');
const fileUtils = require('../utils/upload.utils');
const mail = require('../utils/mail.utils');
const helper = require('../utils/helper.utils');
const auth = require('../utils/auth.utils');
const redis = require('../utils/redis.utils');
const jobUtils = require('../utils/jobs.utils');
const structureUtils = require('../utils/structure.utils');
const { constant } = require('lodash');

const controller = {};

// const structureUtils.adminLoginStructure = (admin) => ({
//     id: admin?._id,
//     firstName: admin?.data?.firstName,
//     userType: admin?.name,
//     lastName: admin?.data?.lastName,
//     email: admin?.data?.email,
//     gender: admin?.data?.gender,
//     phone: {
//         code: admin?.data?.phone.code,
//         number: admin?.data?.phone.number,
//     },
//     avatar: admin?.data?.avatar,
//     accessToken: admin?.data?.accessToken,
//     languageCode: admin?.data?.languageCode,
//     preferredTheme: admin?.data?.preferredTheme ?? '',
// });

controller.checkError = handler(async (req, res) => {
    const data = {
        age: 50,
        name: {
            age: 30,
        },
    };
    if (req.body.type === 'SERVER')
        return res.json({
            age: data.content.age,
        });
    if (req.body.type === 'CLIENT') throw '400|' + constantUtils.INVvALID_CODE;
    else
        return res.json({
            message: 'success!!!',
        });
});

controller.getSpecification = handler(async (req, res) => {
    const data = await si.get({
        currentLoad: 'currentLoad',
        fsSize: '*',
        mem: 'total, free, used',
    });
    return res.json(data);
});

controller.getSocketKeysInfo = handler(async (req, res) => {
    return res.json({
        socket: {
            ty: 'type', // type
            ut: 'userType', // userType
            a: 'action', // action
            ti: 'timeStamp', // timestamp
            m: 'message', // message
            d: 'details', // details
        },
        order: {
            i: 'id', //id
            t: 'orderType', // order type
            s: 'status', // status
            u: {
                // user
                a: 'user.avatar', // avatar
                fn: 'user.firstName', // firstName
                ln: 'user.lastName', // lastName
            },
            pa: {
                // partner
                a: 'user.avatar', // avatar
                fn: 'user.firstName', // firstName
                ln: 'user.lastName', // lastName
            },
            am: 'totalAmount',
            pr: [
                {
                    n: 'product.name',
                    q: 'product.quantity',
                    p: 'product.price',
                },
            ],
            a: {
                // address
                fa: 'address.fullAddress',
                la: 'address.lat',
                lg: 'address.lng',
            },
            p: 'paymentType',
        },
    });
});

controller.getConfig = handler(async (req, res) => {
    const [config, lang, allLanguages] = await parallel([
        function (callback) {
            Other.config().then((data) => {
                callback(null, data);
            });
        },
        function (callback) {
            redis.get(constantUtils.LANGUAGES).then((data) => {
                if (data) {
                    const lang = data.find(
                        (each) => each.languageDefault === true
                    );
                    if (lang)
                        callback(null, {
                            languageName: lang.languageName,
                            languageNativeName: lang?.languageNativeName,
                            languageCode: lang?.languageCode,
                            languageAdminKeys: lang?.languageAdminKeys,
                        });
                    else callback(null, null);
                } else {
                    helper.updateLanguageRedis();
                    Language.findOne(
                        { languageDefault: true },
                        'languageAdminKeys languageName languageNativeName languageCode languageDirection'
                    ).then((data) => {
                        callback(null, data);
                    });
                }
            });
        },
        function (callback) {
            redis.get(constantUtils.LANGUAGES).then((data) => {
                if (data) {
                    callback(
                        null,
                        data.map((each) => ({
                            languageName: each.languageName,
                            languageNativeName: each?.languageNativeName,
                            languageCode: each.languageCode,
                            updatedAt: each.updatedAt,
                        }))
                    );
                } else {
                    helper.updateLanguageRedis();
                    Language.find(
                        {},
                        'updatedAt languageName languageNativeName languageCode'
                    ).then((data) => {
                        callback(
                            null,
                            data.map((each) => ({
                                languageName: each.languageName,
                                languageNativeName: each?.languageNativeName,
                                languageCode: each.languageCode,
                                updatedAt: each.updatedAt,
                            }))
                        );
                    });
                }
            });
        },
    ]);
    if (!config) throw '404|' + constantUtils.CONFIG_NOT_FOUND;
    if (!lang) throw '404|' + constantUtils.NO_DEFAULT_LANGUAGE;
    if (config) {
        return res.json({
            ...config,
            language: lang,
            languages: allLanguages,
        });
    } else throw '404|' + constantUtils.CONFIG_NOT_FOUND;
});

controller.getAllConfig = handler(async (req, res) => {
    const [config, lang, allLanguages] = await parallel([
        function (callback) {
            Other.config().then((data) => {
                callback(null, data);
            });
        },
        function (callback) {
            redis.get(constantUtils.LANGUAGES).then((data) => {
                if (data) {
                    const lang = data.find(
                        (each) => each.languageDefault === true
                    );
                    if (lang)
                        callback(null, {
                            languageName: lang.languageName,
                            languageNativeName: lang?.languageNativeName,
                            languageCode: lang?.languageCode,
                            languageAdminKeys: lang?.languageAdminKeys,
                        });
                    else callback(null, null);
                } else {
                    helper.updateLanguageRedis();
                    Language.findOne(
                        { languageDefault: true },
                        'languageAdminKeys languageName languageNativeName languageCode languageDirection'
                    ).then((data) => {
                        callback(null, data);
                    });
                }
            });
        },
        function (callback) {
            redis.get(constantUtils.LANGUAGES).then((data) => {
                if (data) {
                    callback(
                        null,
                        data.map((each) => ({
                            languageName: each.languageName,
                            languageNativeName: each?.languageNativeName,
                            languageCode: each.languageCode,
                            updatedAt: each.updatedAt,
                        }))
                    );
                } else {
                    helper.updateLanguageRedis();
                    Language.find(
                        {},
                        'updatedAt languageName languageNativeName languageCode'
                    ).then((data) => {
                        callback(
                            null,
                            data.map((each) => ({
                                languageName: each.languageName,
                                languageNativeName: each?.languageNativeName,
                                languageCode: each.languageCode,
                                updatedAt: each.updatedAt,
                            }))
                        );
                    });
                }
            });
        },
    ]);
    if (!config) throw '404|' + constantUtils.CONFIG_NOT_FOUND;
    if (!lang) throw '404|' + constantUtils.NO_DEFAULT_LANGUAGE;
    if (config) {
        return res.json({
            ...config,
            language: lang,
            languages: allLanguages,
        });
    } else throw '404|' + constantUtils.CONFIG_NOT_FOUND;
});

controller.useragent = handler(async (req, res) => {
    return res.json(req.useragent);
});

controller.getMobileConfig = handler(async (req, res) => {
    const languageStructure = (doc) => ({
        languageName: doc.languageName,
        languageNativeName: doc?.languageNativeName,
        languageCode: doc.languageCode,
        updatedAt: doc.updatedAt,
        defaultLanguage: doc.languageDefault,
    });
    const [config, lang] = await parallel([
        function (callback) {
            Other.config().then((data) => {
                if (
                    req.body.secureKey &&
                    data?.secureKey === req.body.secureKey
                ) {
                    callback(null, data);
                } else {
                    callback(null, false);
                }
            });
        },
        function (callback) {
            redis.get(constantUtils.LANGUAGES).then((data) => {
                if (data) {
                    callback(
                        null,
                        data.map((each) => languageStructure(each))
                    );
                } else {
                    helper.updateLanguageRedis();
                    Language.find(
                        {},
                        'updatedAt languageName languageNativeName languageCode defaultLanguage'
                    ).then((data) => {
                        callback(
                            null,
                            data.map((each) => languageStructure(each))
                        );
                    });
                }
            });
        },
    ]);
    if (config === false) throw '400|' + constantUtils.INVALID_SECURE_KEY;
    if (!config) throw '404|' + constantUtils.CONFIG_NOT_FOUND;
    if (!lang) throw '404|' + constantUtils.NO_DEFAULT_LANGUAGE;
    if (config)
        return res.json({
            ...structureUtils.mobileConfigStructure(config),
            languages: lang,
        });
    else throw '404|' + constantUtils.CONFIG_NOT_FOUND;
});

controller.loginValidation = handler(async (req, res) => {
    const activityInsert = req?.body?.activityInsert ?? true;
    const lang = await Language.findOne(
        {
            languageCode: req?.user?.data?.languageCode,
        },
        'languageCode languageName languageNativeName languageAdminKeys languageDirection'
    );
    await Admin.findByIdAndUpdate(
        req.user._id,
        {
            $set: {
                'data.lastLogin': new Date(),
            },
            $inc: {
                'data.loginCount': 1,
            },
        },
        {
            new: true,
        }
    );
    if (activityInsert)
        Activity.create({
            fromId: req.user._id,
            fromType: constantUtils.ACTIVITIES.ADMINS,
            action: constantUtils.ACTIVITIES.LOG_IN,
        });
    return res.json({
        ...structureUtils.adminLoginStructure(req.user),
        language: lang,
    });
});

controller.changePreferredTheme = handler(async (req, res) => {
    if (!req?.user?._id) throw '400|' + constantUtils.USER_NOT_FOUND;
    await Admin.findByIdAndUpdate(req.user._id, {
        $set: {
            'data.preferredTheme': req?.body?.theme,
        },
    });
    return res.json({
        message: 'SUCCESS',
    });
});

controller.updateProfilePicture = handler(async (req, res) => {
    if (req.file && req.user._id) {
        const image = req.file.buffer;
        const oldImage = req?.user?.data?.avatar;
        if (oldImage) await fileUtils.delete(oldImage);
        const fileName = await fileUtils.upload(
            image,
            `${constantUtils.OPERATORS}.${
                req?.user?.data?.phone?.number
            }.${helper.generateRandom(5, '#')}.jpg`
        );
        await Admin.findByIdAndUpdate(req.user._id, {
            $set: {
                'data.avatar': fileName,
            },
        });
        Activity.create({
            fromId: req.user._id,
            fromType: constantUtils.ACTIVITIES.ADMINS,
            action: constantUtils.ACTIVITIES.UPDATE_PROFILE_PICTURE,
        });
        return res.json({
            message: 'Success',
        });
    } else
        return res.json({
            message: 'Image not found',
        });
});

controller.updateProfile = handler(async (req, res) => {
    const ifUser = await Admin.findOne({
        _id: {
            $ne: req.user._id,
        },
        $or: [
            {
                'data.phone.code': req.body.phoneCode,
                'data.phone.number': req.body.phoneNumber,
            },
            { 'data.email': req.body.email },
        ],
    });
    if (ifUser) throw '400|' + constantUtils.PHONE_OR_EMAIL_ALREADY_EXISTS;
    else {
        await Admin.findByIdAndUpdate(req.user._id, {
            $set: {
                'data.firstName': req.body.firstName,
                'data.lastName': req.body.lastName,
                'data.email': req.body.email,
                'data.gender': req.body.gender,
                'data.phone.code': req.body.phoneCode,
                'data.phone.number': req.body.phoneNumber,
                'data.languageCode': req.body.languageCode,
                'data.preferredTheme': req.body.preferredTheme,
                'data.lastEdited': req.user._id,
                'data.lastEditedTime': new Date(),
            },
        });
        Activity.create({
            fromId: req.user._id,
            fromType: constantUtils.ACTIVITIES.ADMINS,
            action: constantUtils.ACTIVITIES.UPDATE_PROFILE,
        });
        return res.json({
            message: 'Success',
        });
    }
});

controller.getProfile = handler(async (req, res) => {
    return res.json(req.user.data);
});

controller.updateProfileLanguage = handler(async (req, res) => {
    const ifLanguage = await Language.findOne({
        languageCode: req.body.languageCode,
    });
    if (!ifLanguage) throw '404|' + constantUtils.INVALID_CODE;
    else {
        await Admin.findByIdAndUpdate(req.user._id, {
            $set: {
                'data.languageCode': req.body.languageCode,
            },
        });
        return res.json(ifLanguage);
    }
});

controller.loginValidate = async (req, admin) => {
    if (auth.comparePassword(req.body.password, admin.data.password)) {
        const token = auth.jwtSign({
            _id: admin._id,
            userType: constantUtils.OPERATORS,
        });
        const updatedAdmin = await Admin.findByIdAndUpdate(
            admin._id,
            {
                $set: {
                    'data.accessToken': token,
                    'data.lastLogin': new Date(),
                },
                $inc: {
                    'data.loginCount': 1,
                },
            },
            {
                new: true,
            }
        );
        const lang = await Language.findOne(
            {
                languageCode: admin?.data?.languageCode,
            },
            'languageCode languageName languageNativeName languageAdminKeys languageDirection'
        );
        return {
            ...structureUtils.adminLoginStructure(updatedAdmin),
            language: lang,
        };
    } else return null;
};

controller.loginWithPhone = handler(async (req, res) => {
    const admin = await Admin.findOne({
        name: { $in: [constantUtils.OPERATORS, constantUtils.DEVELOPER] },
        'data.phone.code': req.body.phoneCode,
        'data.phone.number': req.body.phoneNumber.toString(),
        'data.status': constantUtils.ACTIVE,
    }).lean();
    if (!admin) throw '400|' + constantUtils.USER_OR_PASSWORD_WRONG;
    else {
        const adminData = await controller.loginValidate(req, admin);
        if (!adminData) throw '400|' + constantUtils.INVALID_CREDENTIALS;
        else return res.json(adminData);
    }
});

controller.loginWithEmail = handler(async (req, res) => {
    const admin = await Admin.findOne({
        name: { $in: [constantUtils.OPERATORS, constantUtils.DEVELOPER] },
        'data.email': req.body.email,
        'data.status': constantUtils.ACTIVE,
    });
    if (!admin) throw '400|' + constantUtils.INVALID_CREDENTIALS;
    else {
        const adminData = await controller.loginValidate(req, admin);
        if (!adminData) throw '400|' + constantUtils.INVALID_CREDENTIALS;
        else {
            Activity.create({
                fromId: admin.id,
                fromType: constantUtils.ACTIVITIES.ADMINS,
                action: constantUtils.ACTIVITIES.LOG_IN,
            });
            return res.json(adminData);
        }
    }
});

controller.forgotPasswordWithEmail = handler(async (req, res) => {
    const config = await Other.config();

    const ifAdmin = await Admin.findOne({
        'data.email': req.body.email,
        'data.status': constantUtils.ACTIVE,
    });
    if (!ifAdmin) throw '400|' + constantUtils.USER_NOT_FOUND;

    const otpLength = config?.otpLength ?? 5;

    const otp = helper.generateRandom(otpLength, '#');
    await Admin.findByIdAndUpdate(ifAdmin._id, {
        $set: {
            'data.forgotPasswordOtp': otp,
        },
    });
    mail.sendMail(
        req.body.email,
        'Your OTP is ' + otp,
        `
        <h1>OTP : ${otp}</h1>
    `
    );

    Activity.create({
        fromId: ifAdmin._id,
        fromType: constantUtils.ACTIVITIES.ADMINS,
        action: constantUtils.ACTIVITIES.INITIATED_FORGOT_PASSWORD_EMAIL,
    });

    return res.json({
        message: constantUtils.OTP_SEND,
    });
});

controller.verifyOTPWithEmail = handler(async (req, res) => {
    const { email, otp } = req.body;
    const ifAdmin = await Admin.findOne({
        'data.email': email,
        'data.forgotPasswordOtp': otp,
    });
    if (!ifAdmin) throw '400|' + constantUtils.INVALID_OTP;
    return res.json({
        message: constantUtils.SUCCESS,
    });
});

controller.resetPasswordWithEmail = handler(async (req, res) => {
    const { email, otp, newPassword } = req.body;
    const ifAdmin = await Admin.findOne({
        'data.email': email,
        'data.forgotPasswordOtp': otp,
    });
    if (!ifAdmin) throw '400|' + constantUtils.INVALID_OTP;
    await Admin.findByIdAndUpdate(ifAdmin._id, {
        $set: {
            'data.password': auth.generatePassword(newPassword),
            'data.forgotPasswordOtp': '',
        },
        $inc: {
            'data.loginCount': 1,
        },
    });
    Activity.create({
        fromId: ifAdmin._id,
        fromType: constantUtils.ACTIVITIES.ADMINS,
        action: constantUtils.ACTIVITIES.FORGOT_PASSWORD_COMPLETE,
    });
    return res.json(structureUtils.adminLoginStructure(ifAdmin));
});

controller.updatePassword = handler(async (req, res) => {
    const { oldPassword, newPassword } = req.body;
    const admin = await Admin.findById(req.user._id).lean();
    if (!admin) throw '401|' + constantUtils.INVALID_TOKEN;
    else {
        if (auth.comparePassword(oldPassword, admin.data.password)) {
            await Admin.findByIdAndUpdate(req.user._id, {
                $set: {
                    'data.password': auth.generatePassword(newPassword),
                },
            });
            Activity.create({
                fromId: admin._id,
                fromType: constantUtils.ACTIVITIES.ADMINS,
                action: constantUtils.ACTIVITIES.UPDATE_PASSWORD,
            });
            return res.json({
                code: 200,
                message: constantUtils.SUCCESS,
            });
        } else throw '400|' + constantUtils.OLD_PASSWORD_IS_INVALID;
    }
});

controller.dashboard = handler(async (req, res) => {
    const data = await Order.aggregate([
        {
            $facet: {
                orders: [
                    {
                        $match: {
                            'order.status': {
                                $in: [constantUtils.DELIVERED],
                            },
                        },
                    },
                    {
                        $group: {
                            _id: '$city.id',
                            count: { $sum: 1 },
                        },
                    },
                    { $sort: { count: -1 } },
                    {
                        $lookup: {
                            from: 'dcities',
                            localField: '_id',
                            foreignField: '_id',
                            as: '_id',
                        },
                    },
                    {
                        $unwind: '$_id',
                    },
                    {
                        $project: {
                            _id: 0,
                            locationName: '$_id.locationName',
                            count: 1,
                        },
                    },
                    { $sort: { count: -1 } },
                ],
                products: [
                    {
                        $match: {
                            'order.status': {
                                $in: [constantUtils.DELIVERED],
                            },
                        },
                    },
                    { $unwind: '$product.orderedProducts' },
                    {
                        $group: {
                            _id: '$product.orderedProducts._id',
                            quantity: {
                                $sum: '$product.orderedProducts.quantity',
                            },
                            product: { $first: '$product.orderedProducts' },
                        },
                    },
                    {
                        $sort: { quantity: -1 },
                    },
                    {
                        $project: {
                            _id: 1,
                            quantity: 1,
                            'product.name': 1,
                            'product.image': 1,
                            'product.price': 1,
                            'product.vegNonVeg': 1,
                            'product.cityId': 1,
                        },
                    },
                ],
                users: [
                    {
                        $match: {
                            'order.status': {
                                $in: [constantUtils.DELIVERED],
                            },
                        },
                    },
                    {
                        $group: {
                            _id: '$user.id',
                            city: { $first: '$city.id' },
                            count: { $sum: 1 },
                            amount: { $sum: '$invoice.estimation.total' },
                        },
                    },
                    { $sort: { count: -1 } },
                    { $limit: 5 },
                    {
                        $lookup: {
                            from: 'users',
                            localField: '_id',
                            foreignField: '_id',
                            as: '_id',
                        },
                    },
                    {
                        $unwind: '$_id',
                    },
                    {
                        $lookup: {
                            from: 'dcities',
                            localField: 'city',
                            foreignField: '_id',
                            as: 'city.id',
                        },
                    },
                    {
                        $unwind: '$city.id',
                    },
                    {
                        $project: {
                            _id: 0,
                            id: '$_id._id',
                            firstName: '$_id.firstName',
                            lastName: '$_id.lastName',
                            email: '$_id.email',
                            avatar: '$_id.avatar',
                            phoneCode: '$_id.phone.code',
                            phoneNumber: '$_id.phone.number',
                            currencySymbol: '$city.id.currencySymbol',
                            currencyCode: '$city.id.currencyCode',
                            count: 1,
                            amount: 1,
                        },
                    },
                ],
            },
        },
    ]);
    return res.json(data[0]);
    // const mostActiveOperators = await Admin.find({
    //     _id: {
    //         $ne: req?.user?._id,
    //     },
    //     'data.loginCount': {
    //         $exists: true,
    //     },
    // })
    //     .select(
    //         'data.firstName data.lastName data.phone data.avatar data.loginCount'
    //     )
    //     .sort('-data.loginCount')
    //     .limit(10);
    // return res.json({
    //     mostActiveOperators: mostActiveOperators.map((each) => ({
    //         id: each._id,
    //         firstName: each?.data?.firstName ? each?.data?.firstName : '',
    //         lastName: each?.data?.lastName ? each?.data?.lastName : '',
    //         email: each?.data?.email,
    //         phoneCode: each?.data?.phone?.code,
    //         phoneNumber: each?.data?.phone?.number,
    //         avatar: each?.data?.avatar,
    //         loginCount: each?.data?.loginCount,
    //     })),
    // });
});

// Operators

controller.getAllOperators = handler(async (req, res) => {
    const adminConnections = await redis.get(constantUtils.ADMIN_CONNECTIONS);
    let condition = {
        name: constantUtils.OPERATORS,
        _id: { $ne: req?.user?._id },
    };

    // Sort
    let sort = '-_id';

    // Filters
    if (req?.body?.search)
        condition['$or'] = helper.generateSearch(
            [
                'data.firstName',
                'data.lastName',
                'data.gender',
                'data.email',
                'data.phone.number',
                'data.phone.code',
            ],
            req.body.search
        );
    if (
        req?.body?.filters?.userType === constantUtils.DEVELOPER &&
        req.user.name === constantUtils.DEVELOPER
    ) {
        condition['name'] = constantUtils.DEVELOPER;
    }
    if (req?.body?.filters?.status)
        condition['data.status'] = req.body.filters.status;
    if (req?.body?.filters?.gender)
        condition['data.gender'] = req.body.filters.gender;
    if (req?.body?.filters?.phoneCode)
        condition['data.phone.code'] = req.body.filters.phoneCode;
    if (req?.body?.filters?.sort && req?.body?.filters?.sort === 'LAST_LOGIN') {
        condition['data.lastLogin'] = {
            $exists: true,
        };
        sort = '-data.lastLogin';
    }
    if (req?.body?.filters?.startDate)
        condition['createdAt'] = {
            $gte: req?.body.filters?.startDate,
        };
    if (req?.body?.filters?.endDate)
        condition['createdAt'] = {
            $lte: req?.body?.filters?.endDate,
        };
    if (req?.body?.filters?.login) {
        if (req?.body?.filters?.login === constantUtils.LOGGED)
            condition['data.lastLogin'] = {
                $exists: true,
            };
        if (req?.body?.filters?.login === constantUtils.NOT_LOGGED)
            condition['data.lastLogin'] = {
                $exists: false,
            };
    }

    const count = await Admin.count(condition);
    const operators = await Admin.find(condition)
        .select(
            'data.firstName data.lastName data.email data.avatar data.gender data.status data.phone data.lastLogin data.socketId'
        )
        .sort(sort)
        .skip(helper.getSkip(req))
        .limit(helper.getLimit(req));
    // .exec();

    Activity.create({
        fromId: req.user._id,
        fromType: constantUtils.ACTIVITIES.ADMINS,
        action: constantUtils.ACTIVITIES.VIEW_LIST,
        listType: constantUtils.OPERATORS,
    });

    return res.json({
        totalPages:
            helper.getTotalPages(req, count) === 0
                ? 1
                : helper.getTotalPages(req, count),
        totalDocs: count - 1,
        currentPage: helper.getCurrentPage(req),
        data: operators.map((each) => ({
            id: each._id,
            name: each?.data?.firstName + ' ' + each?.data?.lastName,
            email: each.data.email,
            gender: each.data.gender,
            avatar: each.data.avatar,
            status: each.data.status,
            phoneCode: each?.data?.phone?.code,
            phoneNumber: each?.data?.phone?.number,
            lastLogin: each?.data?.lastLogin,
            online: each?.data?.socketId
                ? adminConnections.includes(each?.data?.socketId)
                : false,
        })),
    });
});

controller.getOperator = handler(async (req, res) => {
    const operator = await Admin.findOne(
        {
            name: { $in: [constantUtils.OPERATORS, constantUtils.DEVELOPER] },
            _id: req?.body?.id,
        },
        '-data.password'
    )
        .populate(
            'data.addedBy',
            '_id data.firstName data.lastName data.email data.avatar'
        )
        .populate(
            'data.lastEdited',
            '_id data.firstName data.lastName data.email data.avatar'
        );
    if (!operator) throw '404|' + constantUtils.NOT_FOUND;
    else {
        Activity.create({
            fromId: req.user._id,
            fromType: constantUtils.ACTIVITIES.ADMINS,
            toId: operator._id,
            toType: constantUtils.ACTIVITIES.ADMINS,
            action: constantUtils.ACTIVITIES.VIEW,
        });
        return res.json(operator);
    }
});

controller.editGetOperator = handler(async (req, res) => {
    const operator = await Admin.findOne({
        name: { $in: [constantUtils.OPERATORS, constantUtils.DEVELOPER] },
        _id: req?.body?.id,
    }).select(
        'data.firstName data.lastName data.gender data.email data.phone data.languageCode data.avatar data.status'
    );
    if (!operator) throw '404|' + constantUtils.INVALID_ID;
    else
        return res.json({
            id: operator?.id,
            firstName: operator?.data?.firstName ?? '',
            lastName: operator?.data?.lastName ?? '',
            gender: operator?.data?.gender ?? '',
            email: operator?.data?.email ?? '',
            phoneCode: operator?.data?.phone?.code ?? '',
            phoneNumber: operator?.data?.phone?.number ?? '',
            language: operator?.data?.languageCode ?? '',
            avatar: operator?.data?.avatar ?? '',
            status: operator?.data?.status,
        });
});

controller.addOperator = handler(async (req, res) => {
    // Image Key Validation
    // if (!helper.ifFileAvailable(req, 'avatar'))
    //     throw '422|' + constantUtils.IMAGE_AVATAR_REQUIRED;
    if (!req.body.avatar && !req.file)
        throw '422|' + constantUtils.IMAGE_AVATAR_REQUIRED;

    if (req.file) {
        const image = req.file.buffer;
        // Deleting Image
        if (req.body.id) {
            const image = await Admin.findById(req.body.id, 'data.avatar');
            if (image && image.data.avatar) {
                await fileUtils.delete(image?.data?.avatar);
            }
        }
        req.body.avatar = await fileUtils.upload(
            image,
            `${constantUtils.OPERATORS}.${
                req.body.phoneNumber
            }.${helper.generateRandom(5, '#')}.jpg`
        );
    }

    const updateData = {
        'data.firstName': req.body.firstName,
        'data.lastName': req.body.lastName,
        'data.email': req.body.email,
        'data.gender': req.body.gender,
        'data.phone': {
            code: req.body.phoneCode,
            number: req.body.phoneNumber.toString(),
        },
        'data.extraPrivileges': {},
        'data.avatar': req.body.avatar,
        'data.accessToken': '',
        'data.languageCode': req.body.languageCode,
        'data.lastEdited': req?.user?._id,
        'data.lastEditedTime': new Date(),
        'data.status': req.body.status,
    };

    let operator;

    // Edit
    if (req.body.id) {
        const ifOperator = await Admin.findOne({
            _id: {
                $ne: req?.body?.id,
            },
            $or: [
                {
                    'data.phone.code': req.body.phoneCode,
                    'data.phone.number': req.body.phoneNumber.toString(),
                },
                { 'data.email': req.body.email },
            ],
        });

        if (ifOperator) throw '400|' + constantUtils.USER_ALREADY_EXISTS;
        operator = await Admin.findByIdAndUpdate(
            req.body.id,
            {
                $set: {
                    ...updateData,
                },
            },
            { new: true }
        );
        Activity.create({
            fromId: req.user._id,
            fromType: constantUtils.ACTIVITIES.ADMINS,
            toId: operator._id,
            toType: constantUtils.ACTIVITIES.ADMINS,
            action: constantUtils.ACTIVITIES.UPDATE,
        });
    }

    // Add
    if (!req.body.id) {
        const ifOperator = await Admin.findOne({
            $or: [
                {
                    'data.phone.code': req.body.phoneCode,
                    'data.phone.number': req.body.phoneNumber.toString(),
                },
                { 'data.email': req.body.email },
            ],
        });

        if (ifOperator) throw '400|' + constantUtils.USER_ALREADY_EXISTS;
        const hashedPassword = auth.generatePassword(req.body.password);
        operator = await Admin.create({
            name: constantUtils.OPERATORS,
            ...updateData,
            'data.status': constantUtils.ACTIVE,
            'data.addedBy': req?.user?._id ?? '',
            'data.password': hashedPassword,
        });
        Activity.create({
            fromId: req.user._id,
            fromType: constantUtils.ACTIVITIES.ADMINS,
            toId: operator._id,
            toType: constantUtils.ACTIVITIES.ADMINS,
            action: constantUtils.ACTIVITIES.ADD,
        });
    }
    return res.json(operator);
});

controller.statusChangeOperator = handler(async (req, res) => {
    await Admin.findOneAndUpdate(
        {
            _id: req.body.id,
            name: constantUtils.OPERATORS,
        },
        {
            $set: {
                'data.status': req.body.status,
            },
        }
    );
    Activity.create({
        fromId: req.user._id,
        fromType: constantUtils.ACTIVITIES.ADMINS,
        toId: req.body.id,
        toType: constantUtils.ACTIVITIES.ADMINS,
        action: constantUtils.ACTIVITIES.STATUS_CHANGE,
    });
    return res.json({
        message: constantUtils.SUCCESS,
    });
});

// ONLY FOR DEVELOPMENT
controller.jobsList = handler(async (req, res) => {
    return res.json(jobUtils.getActiveJobLists());
});

controller.timezoneList = handler(async (req, res) => {
    return res.json(timezones.default.map((each) => each.tzCode).sort());
});

controller.updateGeneralSettings = handler(async (req, res) => {
    const config = await Other.findOne({
        name: constantUtils.GENERALSETTING,
    });
    if (!config) throw constantUtils.CONFIG_NOT_FOUND;

    // Finding Edited Keys
    let editKeys = {};
    editKeys.beforeEditing = helper.differenceInObject(config.data, req.body);
    editKeys.afterEditing = helper.differenceInObject(req.body, config.data);

    const updatedConfig = await Other.findByIdAndUpdate(
        config._id,
        {
            $set: {
                data: req.body,
            },
        },
        { new: true }
    );
    await helper.updateGeneralSettingsRedis();
    Activity.create({
        fromId: req.user._id,
        fromType: 'admins',
        toId: config._id,
        toType: 'others',
        action: constantUtils.ACTIVITIES.UPDATE_GENERAL_SETTINGS,
        editKeys,
    });
    // Socket Emits
    req?.app?.io
        ?.of(constantUtils.NS.ADMINS)
        ?.emit(constantUtils.SOCKET_EVENTS.SETTINGS_UPDATE, true ?? {});
    req?.app?.io
        ?.of(constantUtils.NS.SHOPS)
        ?.emit(constantUtils.SOCKET_EVENTS.SETTINGS_UPDATE, true ?? {});
    req?.app?.io
        ?.of(constantUtils.NS.USERS)
        ?.emit(constantUtils.SOCKET_EVENTS.SETTINGS_UPDATE, true ?? {});
    req?.app?.io
        ?.of(constantUtils.NS.PARTNERS)
        ?.emit(constantUtils.SOCKET_EVENTS.SETTINGS_UPDATE, true ?? {});
    return res.json(editKeys);
});

controller.uploadGeneralSettingsImages = handler(async (req, res) => {
    let imageUrl = {};
    const config = await Other.config();
    if (req?.files?.darkLogo) {
        await fileUtils.delete(config?.darkLogo);
        imageUrl.darkLogo = await fileUtils.upload(
            req?.files?.darkLogo[0]?.buffer,
            `darkLogo.${helper.generateRandom(5, '#')}.jpg`
        );
    }
    if (req?.files?.lightLogo) {
        await fileUtils.delete(config?.lightLogo);
        imageUrl.lightLogo = await fileUtils.upload(
            req?.files?.lightLogo[0]?.buffer,
            `lightLogo.${helper.generateRandom(5, '#')}.jpg`
        );
    }
    if (req?.files?.favicon) {
        await fileUtils.delete(config?.favicon);
        imageUrl.favicon = await fileUtils.upload(
            req?.files?.favicon[0]?.buffer,
            `favicon.${helper.generateRandom(5, '#')}.jpg`
        );
    }
    return res.json(imageUrl);
});

controller.socketLists = handler(async (req, res) => {
    let sockets = await redis.get('ADMIN_CONNECTIONS');
    sockets = sockets.reverse();
    const admins = await Admin.find({
        'data.socketId': { $in: sockets },
    }).select(
        '_id data.firstName data.lastName, data.email data.phone data.socketId'
    );
    return res.json({
        admins: sockets.map((each) =>
            admins.find((eachAdmin) => eachAdmin?.data?.socketId === each)
        ),
    });
});

controller.onlineDetails = handler(async (req, res) => {
    const adminConnections = await redis.get(constantUtils.ADMIN_CONNECTIONS);
    const userConnections = await redis.get(constantUtils.USER_CONNECTIONS);
    const partnerConnections = await redis.get(
        constantUtils.PARTNER_CONNECTIONS
    );

    const [
        operatorsCount,
        operators,
        developersCount,
        developers,
        userCount,
        users,
    ] = await parallel([
        function (callback) {
            try {
                Admin.count({
                    name: constantUtils.OPERATORS,
                    'data.socketId': { $in: adminConnections },
                    _id: { $ne: req?.user?._id },
                }).then((data) => callback(null, data));
            } catch (err) {
                callback(null, null);
            }
        },
        function (callback) {
            try {
                Admin.find({
                    name: constantUtils.OPERATORS,
                    'data.socketId': {
                        $in: adminConnections,
                    },
                    _id: { $ne: req?.user?._id },
                })
                    .select(
                        '_id data.firstName data.lastName data.email data.phone data.avatar data.socketId'
                    )
                    .limit(10)
                    .lean()
                    .then((data) => callback(null, data));
            } catch (err) {
                callback(null, null);
            }
        },
        function (callback) {
            try {
                Admin.count({
                    name: constantUtils.DEVELOPER,
                    'data.socketId': { $in: adminConnections },
                    _id: { $ne: req?.user?._id },
                }).then((data) => callback(null, data));
            } catch (err) {
                callback(null, null);
            }
        },
        function (callback) {
            try {
                Admin.find({
                    name: constantUtils.DEVELOPER,
                    'data.socketId': {
                        $in: adminConnections,
                    },
                    _id: { $ne: req?.user?._id },
                })
                    .select(
                        '_id data.firstName data.lastName data.email data.phone data.avatar data.socketId'
                    )
                    .limit(10)
                    .lean()
                    .then((data) => callback(null, data));
            } catch (err) {
                callback(null, null);
            }
        },
        function (callback) {
            try {
                User.count({
                    'deviceInfo.deliverySocketId': { $in: userConnections },
                }).then((data) => callback(null, data));
            } catch (err) {
                callback(null, null);
            }
        },
        function (callback) {
            try {
                User.find({
                    'deviceInfo.deliverySocketId': {
                        $in: userConnections,
                    },
                })
                    .select(
                        '_id firstName lastName email phone avatar deviceInfo'
                    )
                    .limit(10)
                    .lean()
                    .then((data) => callback(null, data));
            } catch (err) {
                callback(null, null);
            }
        },
    ]);

    return res.json({
        operators: {
            count: operatorsCount ?? 0,
            data: adminConnections
                .reverse()
                .filter((each) =>
                    operators?.find(
                        (eachData) => eachData?.data?.socketId === each
                    )
                )
                .map((each) =>
                    operators?.find(
                        (eachData) => eachData?.data?.socketId === each
                    )
                ),
        },
        developers: {
            count: developersCount ?? 0,
            data: adminConnections
                .reverse()
                .filter((each) =>
                    developers?.find(
                        (eachData) => eachData?.data?.socketId === each
                    )
                )
                .map((each) =>
                    developers?.find(
                        (eachData) => eachData?.data?.socketId === each
                    )
                ),
        },
        users: {
            count: userCount ?? 0,
            data: userConnections
                .reverse()
                .filter((each) =>
                    users?.find(
                        (eachData) =>
                            eachData?.deviceInfo[0]?.deliverySocketId === each
                    )
                )
                .map((each) =>
                    users?.find(
                        (eachData) =>
                            eachData?.deviceInfo[0]?.deliverySocketId === each
                    )
                ),
        },
    });
});

controller.operatorToDeveloper = handler(async (req, res) => {
    const ifOperator = await Admin.findOne({
        name: constantUtils.OPERATORS,
        _id: req.body.id,
    }).select('_id name');
    if (!ifOperator) throw '404|' + constantUtils.DATA_NOT_FOUND;
    await Admin.findByIdAndUpdate(ifOperator._id, {
        $set: {
            name: constantUtils.DEVELOPER,
            'data.lastEdited': req.user._id,
            'data.lastEditedTime': new Date(),
        },
    });
    Activity.create({
        fromId: req.user._id,
        fromType: 'admins',
        toId: req.body.id,
        toType: 'admins',
        action: constantUtils.ACTIVITIES.PROMOTE,
    });
    return res.json({
        message: 'Success',
    });
});

controller.socketEmit = handler(async (req, res) => {
    if (req?.body?.socketId)
        req?.app?.io
            ?.of(req?.body?.namespace)
            ?.to(req?.body?.socketId)
            ?.emit(req?.body?.socketEvent, req?.body?.data);
    else
        req?.app?.io
            ?.of(req?.body?.namespace)
            ?.emit(req?.body?.socketEvent, req?.body?.data);
    return res.json({
        message: 'SENT',
    });
});

controller.restartApp = handler(async (req, res) => {
    const event =
        req?.body?.platform === 'WEB'
            ? constantUtils.SOCKET_EVENTS.RESTART_WEB
            : constantUtils.SOCKET_EVENTS.RESTART_MOBILE;
    if (req?.body?.socketId) {
        req?.app?.io
            ?.of(constantUtils.NS.ADMINS)
            ?.to(req?.body?.socketId)
            ?.emit(event, true);
    } else req?.app?.io?.of(constantUtils.NS.ADMINS)?.emit(event, true);
    if (event === constantUtils.SOCKET_EVENTS.RESTART_WEB)
        Activity.create({
            fromId: req.user._id,
            fromType: 'admins',
            action: constantUtils.ACTIVITIES.FORCE_RESTART_WEB,
        });
    else
        Activity.create({
            fromId: req.user._id,
            fromType: 'admins',
            action: constantUtils.ACTIVITIES.FORCE_RESTART_MOBILE,
        });
    return res.json({
        message: 'Success',
    });
});

controller.updateExpoToken = handler(async (req, res) => {
    const token = req?.body?.expoToken;
    if (token) {
        await Admin.updateOne(
            {
                _id: req?.user?._id,
            },
            {
                $set: {
                    'data.expoToken': token,
                },
            }
        );
        return res.json({
            message: 'SUCCESS',
        });
    } else throw '400|' + constantUtils.INVALID_CODE;
});

controller.getNotifications = handler(async (req, res) => {
    let condition = {
        $or: [
            {
                notificationTargetType: constantUtils.ALL,
                userType: constantUtils.NOTIFICATIONS.ADMINS,
            },
            {
                userId: req?.user?._id,
                notificationTargetType: constantUtils.SINGLE,
                userType: constantUtils.NOTIFICATIONS.ADMINS,
            },
        ],
        // name: constantUtils.OPERATORS,
        // _id: { $ne: req?.user?._id },
    };

    // Sort
    let sort = '-_id';

    const [count, notifications] = await Promise.all([
        Notification.count(condition),
        Notification.aggregate([
            {
                $match: condition,
            },
            {
                $sort: {
                    _id: -1,
                },
            },
            {
                $skip: helper.getSkip(req),
            },
            {
                $limit: helper.getLimit(req),
            },
            {
                $lookup: {
                    from: 'dpartners',
                    localField: 'notificationContent.partnerId',
                    foreignField: '_id',
                    as: 'notificationContent.partnerId',
                },
            },
            {
                $unwind: '$notificationContent.partnerId',
            },
        ]),
    ]);

    return res.json({
        totalPages:
            helper.getTotalPages(req, count) === 0
                ? 1
                : helper.getTotalPages(req, count),
        totalDocs: count - 1,
        currentPage: helper.getCurrentPage(req),
        data: notifications,
    });
});

controller.webHook = handler(async (req, res) => {
    helper.consoleLog({
        title: 'WebHook Check',
        data: req?.body,
    });
    return res.json({
        message: 'working',
    });
});

module.exports = controller;
