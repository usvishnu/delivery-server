const handler = require('express-async-handler');
const { v4: uuid } = require('uuid');
const parallel = require('async/parallel');

const User = require('../models/user.model');
const Shop = require('../models/shop.model');
const Other = require('../models/other.model');
const Activity = require('../models/activity.model');
const Language = require('../models/language.model');
const Catalogue = require('../models/catalogue.model');
const Category = require('../models/category.model');
const Product = require('../models/product.model');
const Partner = require('../models/partner.model.js');

const auth = require('../utils/auth.utils');
const smsUtils = require('../utils/sms.utils');
const helper = require('../utils/helper.utils');
const mail = require('../utils/mail.utils');
const redis = require('../utils/redis.utils');

const constantUtils = require('../utils/constant.utils');
const fileUtils = require('../utils/upload.utils');
const structureUtils = require('../utils/structure.utils');

const controller = {};

const loginStructure = (shop) => ({
    shopName: shop?.shopName,
    shopType: shop?.shopType,
    email: shop?.email,
    gender: shop?.gender,
    address: shop?.address,
    head: shop?.headId,
    city: shop?.cityId,
    zone: shop?.zoneId,
    catalogue: shop?.catalogueId,
    preferredTheme: shop?.preferredTheme,
    phone: {
        code: shop?.phone?.code,
        number: shop?.phone?.number,
    },
    avatar: shop?.avatar,
    accessToken: shop?.accessToken,
    languageCode: shop?.languageCode,
});

controller.loginValidate = async (req, shop) => {
    if (auth.comparePassword(req.body.password, shop.password)) {
        const token = auth.jwtSign({
            _id: shop._id,
            userType: constantUtils.SHOPS,
        });
        const updatedShop = await Shop.findByIdAndUpdate(
            shop._id,
            {
                $set: {
                    accessToken: token,
                    lastLogin: new Date(),
                },
                $inc: {
                    loginCount: 1,
                },
            },
            {
                new: true,
            }
        )
            .populate('cityId')
            .populate('zoneId')
            .populate('headId', 'avatar email phone shopName')
            .populate('catalogueId', 'name');
        const lang = await Language.findOne(
            {
                languageCode: shop?.languageCode,
            },
            'languageCode languageName languageNativeName languageAdminKeys languageDirection'
        );
        return { ...loginStructure(updatedShop), language: lang };
    } else return null;
};

controller.getAllShops = handler(async (req, res) => {
    let condition = {
        shopType: constantUtils.RESTAURANTS,
        $or: helper.generateSearch(
            ['shopName', 'email', 'phone.number', 'phone.code'],
            req.body.search
        ),
    };

    // Sort
    let sort = '-_id';

    // Filters
    if (req?.body?.filters?.status)
        condition['status'] = req.body.filters.status;
    if (req?.body?.filters?.shopType)
        condition['shopType'] = req.body.filters.shopType;
    if (req?.body?.filters?.startDate)
        condition['createdAt'] = {
            $gte: req?.body.filters?.startDate,
        };
    if (req?.body?.filters?.endDate)
        condition['createdAt'] = {
            $lte: req?.body?.filters?.endDate,
        };
    if (req?.body?.filters?.headType === 'INDEPENDENT')
        condition['headId'] = { $exists: false };
    if (req?.body?.filters?.headType === 'HEADS')
        condition['headId'] = { $exists: true };

    const count = await Shop.count(condition);
    const shops = await Shop.find(condition)
        .select(
            'shopType phone avatar status shopName email createdAt address headId'
        )
        .sort(sort)
        .skip(helper.getSkip(req))
        .limit(helper.getLimit(req))
        .lean();

    Activity.create({
        fromId: req.user._id,
        fromType: constantUtils.ACTIVITIES.ADMINS,
        action: constantUtils.ACTIVITIES.VIEW_LIST,
        listType: constantUtils.SHOPS,
    });

    return res.json({
        totalPages: helper.getTotalPages(req, count),
        totalDocs: count,
        currentPage: helper.getCurrentPage(req),
        data: shops,
    });
});

controller.convertShopToHead = handler(async (req, res) => {
    const updateData = {
        shopName: req?.body?.name,
        shopType: constantUtils.HEADS,
        email: req?.body?.email,
        password: req?.body?.password,
        'phone.code': req?.body?.phoneCode,
        'phone.number': req?.body?.phoneNumber,
        lastEditedTime: new Date(),
        languageCode: 'en',
        lastEdited: req?.user?._id,
        password: auth.generatePassword(req?.body?.password),
        addedBy: req?.user?._id,
    };

    const shop = await Shop.findOne({
        _id: req?.body?.shopId,
        shopType: constantUtils.RESTAURANTS,
    });
    if (!shop) throw '400|' + constantUtils.SHOP_NOT_FOUND;

    const ifShopExists = await Shop.count({
        $or: [
            {
                email: req?.body?.email,
            },
            {
                'phone.number': req?.body?.phoneNumber,
            },
        ],
    });
    if (ifShopExists)
        throw '400|' + constantUtils.PHONE_OR_EMAIL_ALREADY_EXISTS;

    if (req?.file) {
        const url = await fileUtils.upload(
            req?.file?.buffer,
            `shop.heads.${helper.generateRandom(10, '#')}.jpg`
        );
        updateData['avatar'] = url;
    }

    const head = await Shop.create(updateData);
    shop.headId = head._id;

    await Catalogue.findOneAndUpdate(
        {
            _id: shop?.catalogueId,
            owners: { $ne: head._id },
        },
        {
            $set: {
                headId: head._id,
            },
            $push: {
                owners: head._id,
            },
        }
    );

    await shop.save();

    return res.json(head);
});

controller.getHeadsNames = handler(async (req, res) => {
    const heads = await Shop.find({
        shopType: constantUtils.HEADS,
    })
        .select('_id shopName avatar')
        .lean();
    return res.json(heads);
});

controller.getHeadsCatalogues = handler(async (req, res) => {
    const catalogues = await Catalogue.find({
        headId: req?.body?.id,
    });
    return res.json(catalogues);
});

controller.addShopToHead = handler(async (req, res) => {
    const shop = await Shop.findOne({
        _id: req?.body?.shopId,
        shopType: { $ne: constantUtils.HEADS },
        headId: { $exists: false },
    });
    if (!shop) throw '400|' + constantUtils.SHOP_NOT_FOUND;

    const head = await Shop.findOne({
        _id: req?.body?.headId,
        shopType: constantUtils.HEADS,
    });
    if (!head) throw '400|' + constantUtils.HEAD_NOT_FOUND;

    shop.headId = head._id;
    if (req?.body?.catalogueId) {
        // New Catalogue ID Comes
        if (shop?.catalogueId !== req?.body?.catalogueId) {
            shop.catalogueId = req?.body?.catalogueId;
        }
    }

    await shop.save();

    await Catalogue.findOneAndUpdate(
        {
            _id: req?.body?.catalogueId ?? shop?.catalogueId,
            owners: { $ne: head._id },
        },
        {
            $set: {
                headId: head._id,
            },
            $push: {
                owners: head._id,
            },
        }
    );

    return res.json(head);
});

controller.getAllHeads = handler(async (req, res) => {
    let condition = {
        shopType: constantUtils.HEADS,
        $or: helper.generateSearch(
            ['shopName', 'email', 'phone.number', 'phone.code'],
            req.body.search
        ),
    };

    // Sort
    let sort = '-_id';

    // Filters
    if (req?.body?.filters?.status)
        condition['status'] = req.body.filters.status;
    if (req?.body?.filters?.startDate)
        condition['createdAt'] = {
            $gte: req?.body.filters?.startDate,
        };
    if (req?.body?.filters?.endDate)
        condition['createdAt'] = {
            $lte: req?.body?.filters?.endDate,
        };

    const count = await Shop.count(condition);
    const shops = await Shop.find(condition)
        .select('shopType phone avatar status shopName email createdAt address')
        .sort(sort)
        .skip(helper.getSkip(req))
        .limit(helper.getLimit(req))
        .lean();

    // Activity.create({
    //     fromId: req.user._id,
    //     fromType: constantUtils.ACTIVITIES.ADMINS,
    //     action: constantUtils.ACTIVITIES.VIEW_LIST,
    //     listType: constantUtils.SHOPS,
    // });

    return res.json({
        totalPages: helper.getTotalPages(req, count),
        totalDocs: count,
        currentPage: helper.getCurrentPage(req),
        data: shops,
    });
});

controller.getHead = handler(async (req, res) => {
    const head = await Shop.findOne({
        _id: req?.body?.id,
        shopType: constantUtils.HEADS,
    })
        .populate(
            'lastEdited',
            'name data.firstName data.lastName data.email data.phone data.avatar'
        )
        .populate(
            'addedBy',
            'name data.firstName data.lastName data.email data.phone data.avatar'
        )
        .lean();
    const catalogues = await Catalogue.find({
        headId: req?.body?.id,
    }).lean();
    const shops = await Shop.find({
        headId: req?.body?.id,
    })
        .populate('cityId', '_id locationName')
        .populate('zoneId', '_id zoneName')
        .populate('catalogueId', '_id name')
        .lean();
    return res.json({ ...head, catalogues, shops });
});

controller.addShop = handler(async (req, res) => {
    const updateData = {
        cityId: req.body.cityId,
        zoneId: req.body.zoneId,
        shopName: req.body.shopName,
        avatar: req.body.avatar,
        email: req.body.email,
        'phone.code': req.body.phoneCode,
        'phone.number': req.body.phoneNumber,
        languageCode: req.body.languageCode,
        'address.placeId': req?.body?.address?.placeId,
        'address.addressName': req?.body?.address?.addressName,
        'address.fullAddress': req?.body?.address?.fullAddress,
        'address.shortAddress': req?.body?.address?.shortAddress,
        'address.lat': req?.body?.address?.lat,
        'address.lng': req?.body?.address?.lng,
        onlineStatus: req?.body?.onlineStatus,
        'serviceAvailableTime.days': req?.body?.availableDays,
        'serviceAvailableTime.startTime': req?.body?.startTime,
        'serviceAvailableTime.endTime': req?.body?.endTime,
        'location.coordinates': [
            req?.body?.address?.lng,
            req?.body?.address?.lat,
        ],
        'location.type': 'Point',
        shopType: constantUtils.RESTAURANTS,
        lastEdited: req.user._id,
        lastEditedTime: new Date(),
    };

    // Edit
    if (req?.body?.id) {
        const ifShop = await Shop.count({
            _id: {
                $ne: req?.body?.id,
            },
            $or: [
                {
                    'phone.code': req.body.phoneCode,
                    'phone.number': req.body.phoneNumber.toString(),
                },
                { email: req.body.email },
            ],
        });
        if (ifShop > 0) throw '400|' + constantUtils.USER_ALREADY_EXISTS;

        let catalogue = {};
        if (req?.body?.catalogueType === 'NEW') {
            Activity.create({
                fromId: req.user._id,
                fromType: constantUtils.ACTIVITIES.ADMINS,
                action: constantUtils.ACTIVITIES.ADD,
                toId: catalogue._id,
                toType: constantUtils.ACTIVITIES.CATALOGUES,
            });
            catalogue = await Catalogue.create({
                name: req?.body?.catalogueName,
                owners: [req?.body?.id],
                // cities: [
                //     {
                //         cityId: req?.body?.cityId,
                //     },
                // ],
                status: constantUtils.ACTIVE,
                addedBy: req?.user?._id,
                lastEdited: req?.user?._id,
                lastEditedTime: new Date(),
            });
        } else catalogue._id = req?.body?.catalogueId;

        const shop = await Shop.findByIdAndUpdate(
            req?.body?.id,
            {
                $set: {
                    ...updateData,
                    catalogueId: catalogue._id,
                },
            },
            {
                new: true,
            }
        );
        if (!shop) throw '400|' + constantUtils.DATA_NOT_FOUND;
        Activity.create({
            fromId: req.user._id,
            fromType: constantUtils.ACTIVITIES.ADMINS,
            action: constantUtils.ACTIVITIES.UPDATE,
            toId: shop._id,
            toType: constantUtils.ACTIVITIES.SHOPS,
        });
        return res.json(shop);
    } else {
        const ifShop = await Shop.count({
            $or: [
                {
                    'phone.code': req.body.phoneCode,
                    'phone.number': req.body.phoneNumber.toString(),
                },
                { email: req.body.email },
            ],
        });
        if (ifShop > 0) throw '400|' + constantUtils.USER_ALREADY_EXISTS;
        const hashedPassword = auth.generatePassword(req.body.password);

        let catalogue = {};
        if (req?.body?.catalogueType === 'NEW') {
            catalogue = await Catalogue.create({
                name: req?.body?.catalogueName,
                // cities: [
                //     {
                //         cityId: req?.body?.cityId,
                //     },
                // ],
                status: constantUtils.ACTIVE,
                addedBy: req?.user?._id,
                lastEdited: req?.user?._id,
                lastEditedTime: new Date(),
            });
            Activity.create({
                fromId: req.user._id,
                fromType: constantUtils.ACTIVITIES.ADMINS,
                action: constantUtils.ACTIVITIES.ADD,
                toId: catalogue._id,
                toType: constantUtils.ACTIVITIES.CATALOGUES,
            });
        } else catalogue._id = req?.body?.catalogueId;

        if (req?.body?.headId) updateData['headId'] = req?.body?.headId;

        const shop = await Shop.create({
            ...updateData,
            onlineStatus: false,
            catalogueId: catalogue._id,
            password: hashedPassword,
            status: constantUtils.ACTIVE,
            addedBy: req.user._id,
            status: constantUtils.ACTIVE,
        });
        if (req?.body?.catalogueType === 'NEW')
            await Catalogue.updateOne(
                {
                    _id: catalogue._id,
                },
                {
                    $set: {
                        owners: [shop._id],
                    },
                }
            );
        Activity.create({
            fromId: req.user._id,
            fromType: constantUtils.ACTIVITIES.ADMINS,
            action: constantUtils.ACTIVITIES.ADD,
            toId: shop._id,
            toType: constantUtils.ACTIVITIES.SHOPS,
        });
        return res.json(shop);
    }
});

controller.loginWithEmail = handler(async (req, res) => {
    const shop = await Shop.findOne({
        email: req.body.email,
        status: constantUtils.ACTIVE,
    });
    if (!shop) throw '400|' + constantUtils.INVALID_CREDENTIALS;
    else {
        const shopData = await controller.loginValidate(req, shop);
        if (!shopData) throw '400|' + constantUtils.INVALID_CREDENTIALS;
        else {
            Activity.create({
                fromId: shop._id,
                fromType: constantUtils.ACTIVITIES.SHOPS,
                action: constantUtils.ACTIVITIES.LOG_IN,
            });
            return res.json(shopData);
        }
    }
});

controller.getDashboardData = handler(async (req, res) => {
    const config = await Other.config();
    let headShops = [];
    let partnersLowOnStock = [];
    let reFillRequestedPartners = [];

    if (req?.user?.shopType === constantUtils.HEADS) {
        headShops = await Shop.find({
            shopType: { $ne: constantUtils.HEADS },
            headId: req?.user?._id,
        }).select('-password');
    }

    if (config?.partner?.showStockPartnerInAllShops ?? true) {
        partnersLowOnStock = await Partner.find({
            partnerType: constantUtils.STOCK_PARTNER,
            status: constantUtils.ACTIVE,
            'availableProducts.stockQuantity': {
                $lt: config?.partner?.minimumStock ?? 10,
            },
        })
            .select(
                '_id createdAt firstName lastName email phone avatar reFillRequest'
            )
            .sort('-_id');
    }

    if (config?.partner?.showStockPartnerInAllShops ?? true) {
        reFillRequestedPartners = await Partner.find({
            partnerType: constantUtils.STOCK_PARTNER,
            status: constantUtils.ACTIVE,
            'reFillRequest.status': constantUtils.NEW,
        })
            .select(
                '_id createdAt firstName lastName email phone avatar reFillRequest'
            )
            .sort('reFillRequest.newTime');
    }

    return res.json({
        headShops,
        partnersLowOnStock,
        reFillRequestedPartners,
    });
});

controller.getPartnersLists = handler(async (req, res) => {
    const partnerConnections = await redis.get(
        constantUtils.PARTNER_CONNECTIONS
    );
    let condition = {
        $or: helper.generateSearch(
            ['firstName', 'lastName', 'email', 'phone.number', 'phone.code'],
            req.body.search
        ),
    };

    // Sort
    let sort = '-_id';

    // Filters
    if (req?.body?.filters?.status)
        condition['status'] = req.body.filters.status;
    if (req?.body?.filters?.gender)
        condition['gender'] = req.body.filters.gender;
    if (req?.body?.filters?.startDate)
        condition['createdAt'] = {
            $gte: req?.body.filters?.startDate,
        };
    if (req?.body?.filters?.endDate)
        condition['createdAt'] = {
            $lte: req?.body?.filters?.endDate,
        };

    const count = await Partner.count(condition);
    const partners = await Partner.find(condition)
        .select(
            'firstName lastName partnerType email reFillRequest phone avatar createdAt status onlineStatus deviceInfo'
        )
        .sort(sort)
        .skip(helper.getSkip(req))
        .limit(helper.getLimit(req))
        .lean();

    // Activity.create({
    //     fromId: req.user._id,
    //     fromType: constantUtils.ACTIVITIES.ADMINS,
    //     action: constantUtils.ACTIVITIES.VIEW_LIST,
    //     listType: constantUtils.PARTNERS,
    // });

    return res.json({
        totalPages: helper.getTotalPages(req, count),
        totalDocs: count,
        currentPage: helper.getCurrentPage(req),
        data: partners.map((each) => ({
            id: each?._id,
            partnerType: each?.partnerType,
            firstName: each?.firstName,
            lastName: each?.lastName,
            email: each?.email,
            reFillRequest: each?.reFillRequest,
            avatar: each?.avatar,
            joined: each?.createdAt,
            phone: each?.phone,
            onlineStatus: each?.onlineStatus,
            status: each?.status,
            socketConnected: each?.deviceInfo?.[0]?.deliverySocketId
                ? partnerConnections.includes(
                      each?.deviceInfo?.[0]?.deliverySocketId
                  )
                : false,
        })),
    });
});

controller.getPartnerView = handler(async (req, res) => {
    const partner = await Partner.findById(req?.body?.id ?? null)
        .populate('cityId')
        .populate('zones.zoneId')
        .populate('availableProducts.productId')
        .populate('vehicles.vehicleId');
    return res.json(partner);
});

controller.loginValidation = handler(async (req, res) => {
    const activityInsert = req?.body?.activityInsert ?? true;
    const shop = await Shop.findById(req?.user?._id)
        .populate('cityId')
        .populate('zoneId')
        .populate('headId', 'avatar email phone shopName')
        .populate('catalogueId', 'name');
    const lang = await Language.findOne(
        {
            languageCode: req?.user?.languageCode,
        },
        'languageCode languageName languageNativeName languageAdminKeys languageDirection'
    );
    await Shop.findByIdAndUpdate(
        req.user._id,
        {
            $set: {
                'data.lastLogin': new Date(),
            },
            $inc: {
                'data.loginCount': 1,
            },
        },
        {
            new: true,
        }
    );
    if (activityInsert)
        Activity.create({
            fromId: req.user._id,
            fromType: constantUtils.ACTIVITIES.SHOPS,
            action: constantUtils.ACTIVITIES.LOG_IN,
        });
    return res.json({ ...loginStructure(shop), language: lang });
});

controller.getShop = handler(async (req, res) => {
    if (req?.body?.id) {
        const shop = await Shop.findById(req?.body?.id)
            .populate('cityId', '_id currencySymbol locationName location')
            .populate('zoneId', '_id zoneName location')
            .populate('headId', '_id shopName avatar')
            .populate('catalogueId')
            .populate(
                'lastEdited',
                'name data.firstName data.lastName data.email data.phone data.avatar'
            )
            .populate(
                'addedBy',
                'name data.firstName data.lastName data.email data.phone data.avatar'
            );
        if (!shop) throw '400|' + constantUtils.DATA_NOT_FOUND;
        else {
            if (req?.body?.activity !== false)
                Activity.create({
                    fromId: req.user._id,
                    fromType: constantUtils.ACTIVITIES.ADMINS,
                    action: constantUtils.ACTIVITIES.VIEW,
                    toId: shop._id,
                    toType: constantUtils.ACTIVITIES.SHOPS,
                });
            return res.json(shop);
        }
    } else throw '400|' + constantUtils.INVALID_ID;
});

controller.uploadImage = handler(async (req, res) => {
    if (req?.file) {
        const url = await fileUtils.upload(
            req?.file?.buffer,
            `shop.${helper.generateRandom(10, '#')}.jpg`
        );
        return res.json(url);
    } else {
        return res.json('');
    }
});

controller.addCatalogue = handler(async (req, res) => {
    const updateData = {
        name: req.body.name,
        lastEdited: req.user._id,
        lastEditedTime: new Date(),
    };
    let catalogue = {};

    if (req.body?.id) {
        catalogue = await Catalogue.findByIdAndUpdate(
            req?.body?.id,
            {
                $set: {
                    ...updateData,
                },
            },
            { new: true }
        );
    } else {
        catalogue = await Catalogue.create({
            ...updateData,
            addedBy: req?.user?._id,
            status: constantUtils.ACTIVE,
        });
        Activity.create({
            fromId: req.user._id,
            fromType: constantUtils.ACTIVITIES.ADMINS,
            action: constantUtils.ACTIVITIES.CREATE,
            toId: catalogue._id,
            toType: constantUtils.ACTIVITIES.CATALOGUES,
        });
        await Shop.findByIdAndUpdate(req?.body?.shopId, {
            $set: {
                catalogueId: catalogue._id,
            },
        });
    }
    return res.json(catalogue);
});

controller.addCategory = handler(async (req, res) => {
    if (!req.body.image && !req.files?.image)
        throw '422|' + constantUtils.IMAGE_AVATAR_REQUIRED;

    if (req.files?.image) {
        const image = req.files?.image?.[0]?.buffer;
        // Deleting Image
        if (req?.body?.id) {
            const image = await Category.findById(req?.body?.id, 'image');
            if (image && image?.image) {
                await fileUtils.delete(image.image);
            }
        }
        req.body.image = await fileUtils.upload(
            image,
            `${constantUtils.CATALOGUE}.${helper.generateRandom(30, '#')}.jpg`
        );
    }
    if (req.files?.secondaryImage) {
        const image = req.files?.secondaryImage?.[0]?.buffer;
        // Deleting Image
        if (req?.body?.id) {
            const image = await Category.findById(
                req?.body?.id,
                'secondaryImage'
            );
            if (image && image?.secondaryImage) {
                await fileUtils.delete(image.secondaryImage);
            }
        }
        req.body.secondaryImage = await fileUtils.upload(
            image,
            `${constantUtils.CATALOGUE}.${helper.generateRandom(30, '#')}.jpg`
        );
    }
    const updateData = {
        catalogueId: req?.body?.catalogueId,
        name: req.body.name,
        cities: req?.body?.cities
            ? req?.body?.cities?.map((each) => ({ cityId: each }))
            : [],
        image: req.body.image,
        secondaryImage: req?.body?.secondaryImage,
        lastEdited: req.user._id,
        lastEditedTime: new Date(),
    };
    let category = {};

    if (req.body?.id) {
        category = await Category.findByIdAndUpdate(
            req?.body?.id,
            {
                $set: {
                    ...updateData,
                },
            },
            { new: true }
        );
        Activity.create({
            fromId: req.user._id,
            fromType: constantUtils.ACTIVITIES.ADMINS,
            action: constantUtils.ACTIVITIES.UPDATE,
            toId: category._id,
            toType: constantUtils.ACTIVITIES.CATEGORIES,
        });
    } else {
        category = await Category.create({
            ...updateData,
            categoryType: 'CATEGORY',
            addedBy: req?.user?._id,
            status: constantUtils.ACTIVE,
        });
        if (req?.body?.contains === 'PRODUCTS') {
            const subCategory = await Category.create({
                name: 'BYPASS_SUB_CATEGORY',
                catalogueId: req?.body?.catalogueId,
                categoryId: category._id,
                byPassSubcategory: true,
                image: '',
                lastEdited: req.user._id,
                lastEditedTime: new Date(),
                categoryType: 'SUBCATEGORY',
                addedBy: req?.user?._id,
                status: constantUtils.ACTIVE,
            });
            await Category.findByIdAndUpdate(category._id, {
                $push: {
                    subCategories: subCategory._id,
                },
            });
        }
        Activity.create({
            fromId: req.user._id,
            fromType: constantUtils.ACTIVITIES.ADMINS,
            action: constantUtils.ACTIVITIES.CREATE,
            toId: category._id,
            toType: constantUtils.ACTIVITIES.CATEGORIES,
        });
        await Catalogue.findByIdAndUpdate(req?.body?.catalogueId, {
            $push: {
                categories: category._id,
            },
        });
    }
    await Category.updateMany(
        {
            categoryId: category._id,
        },
        {
            $set: {
                cities: req?.body?.cities
                    ? req?.body?.cities?.map((each) => ({ cityId: each }))
                    : [],
            },
        }
    );
    await Product.updateMany(
        {
            categoryId: category._id,
        },
        {
            $set: {
                cities: req?.body?.cities
                    ? req?.body?.cities?.map((each) => ({ cityId: each }))
                    : [],
            },
        }
    );
    return res.json(category);
});

controller.addSubcategories = handler(async (req, res) => {
    if (!req.body.image && !req.files?.image)
        throw '422|' + constantUtils.IMAGE_AVATAR_REQUIRED;

    if (req.files?.image) {
        const image = req.files?.image?.[0]?.buffer;
        // Deleting Image
        if (req?.body?.id) {
            const image = await Category.findById(req?.body?.id, 'image');
            if (image && image?.image) {
                await fileUtils.delete(image.image);
            }
        }
        req.body.image = await fileUtils.upload(
            image,
            `${constantUtils.CATALOGUE}.${helper.generateRandom(30, '#')}.jpg`
        );
    }
    if (req.files?.secondaryImage) {
        const image = req.files?.secondaryImage?.[0]?.buffer;
        // Deleting Image
        if (req?.body?.id) {
            const image = await Category.findById(
                req?.body?.id,
                'secondaryImage'
            );
            if (image && image?.secondaryImage) {
                await fileUtils.delete(image.secondaryImage);
            }
        }
        req.body.secondaryImage = await fileUtils.upload(
            image,
            `${constantUtils.CATALOGUE}.${helper.generateRandom(30, '#')}.jpg`
        );
    }
    let subCategory = {};

    const updateData = {
        categoryId: req?.body?.categoryId,
        name: req?.body?.name,
        cities: req?.body?.cities
            ? req?.body?.cities?.map((each) => ({ cityId: each }))
            : [],
        catalogueId: req?.body?.catalogueId,
        secondaryImage: req?.body?.secondaryImage,
        image: req?.body?.image,
        lastEdited: req.user._id,
        lastEditedTime: new Date(),
    };

    if (req.body?.id) {
        subCategory = await Category.findByIdAndUpdate(
            req?.body?.id,
            {
                $set: {
                    ...updateData,
                },
            },
            { new: true }
        );
        Activity.create({
            fromId: req.user._id,
            fromType: constantUtils.ACTIVITIES.ADMINS,
            action: constantUtils.ACTIVITIES.UPDATE,
            toId: subCategory._id,
            toType: constantUtils.ACTIVITIES.CATEGORIES,
        });
    } else {
        subCategory = await Category.create({
            ...updateData,
            categoryType: 'SUBCATEGORY',
            addedBy: req?.user?._id,
            status: constantUtils.ACTIVE,
        });
        Activity.create({
            fromId: req.user._id,
            fromType: constantUtils.ACTIVITIES.ADMINS,
            action: constantUtils.ACTIVITIES.CREATE,
            toId: subCategory._id,
            toType: constantUtils.ACTIVITIES.CATEGORIES,
        });
        await Category.findByIdAndUpdate(req?.body?.categoryId, {
            $push: {
                subCategories: subCategory._id,
            },
        });
    }
    return res.json(subCategory);
});

controller.addProducts = handler(async (req, res) => {
    if (!req.body.image && !req.files?.image)
        throw '422|' + constantUtils.IMAGE_AVATAR_REQUIRED;

    if (req.files?.image) {
        const image = req.files?.image?.[0]?.buffer;
        // Deleting Image
        if (req?.body?.id) {
            const image = await Product.findById(req?.body?.id, 'image');
            if (image && image?.image) {
                await fileUtils.delete(image.image);
            }
        }
        req.body.image = await fileUtils.upload(
            image,
            `${constantUtils.CATALOGUE}.${helper.generateRandom(30, '#')}.jpg`
        );
    }
    if (req.files?.secondaryImage) {
        const image = req.files?.secondaryImage?.[0]?.buffer;
        // Deleting Image
        if (req?.body?.id) {
            const image = await Product.findById(
                req?.body?.id,
                'secondaryImage'
            );
            if (image && image?.secondaryImage) {
                await fileUtils.delete(image.secondaryImage);
            }
        }
        req.body.secondaryImage = await fileUtils.upload(
            image,
            `${constantUtils.CATALOGUE}.${helper.generateRandom(30, '#')}.jpg`
        );
    }
    let product = {};

    const updateData = {
        catalogueId: req?.body?.catalogueId,
        categoryId: req?.body?.categoryId,
        subCategoryId: req?.body?.subCategoryId,
        name: req?.body?.name,
        cities: req?.body?.cities
            ? req?.body?.cities?.map((each) => ({ cityId: each }))
            : [],
        productType: req?.body?.productType,
        secondaryImage: req?.body?.secondaryImage,
        maxCartLimit: req?.body?.maxCartLimit,
        tags: req?.body?.tags,
        image: req?.body?.image,
        price: req?.body?.price,
        description: req?.body?.description ?? '',
        vegNonVeg: req?.body?.vegNonVeg,
        lastEdited: req.user._id,
        lastEditedTime: new Date(),
    };

    if (req.body?.id) {
        product = await Product.findByIdAndUpdate(
            req?.body?.id,
            {
                $set: {
                    ...updateData,
                },
            },
            { new: true }
        );
        if (req?.body?.productType === constantUtils.QUANTITY_STOCK) {
            const partners = await Partner.find({
                catalogueId: req?.body?.catalogueId,
                partnerType: constantUtils.STOCK_PARTNER,
                availableProducts: {
                    $elemMatch: {
                        productId: req?.body?.id,
                        stockQuantity: { $exists: false },
                    },
                },
            });
            await Partner.updateMany(
                {
                    catalogueId: req?.body?.catalogueId,
                    partnerType: constantUtils.STOCK_PARTNER,
                    availableProducts: {
                        $elemMatch: {
                            productId: req?.body?.id,
                            stockQuantity: { $exists: false },
                        },
                    },
                },
                {
                    $set: {
                        'availableProducts.$.stockQuantity': 0,
                    },
                }
            );
        }
        Activity.create({
            fromId: req.user._id,
            fromType: constantUtils.ACTIVITIES.ADMINS,
            action: constantUtils.ACTIVITIES.UPDATE,
            toId: product._id,
            toType: constantUtils.ACTIVITIES.PRODUCTS,
        });
    } else {
        product = await Product.create({
            ...updateData,
            addedBy: req?.user?._id,
            status: constantUtils.ACTIVE,
        });
        Activity.create({
            fromId: req.user._id,
            fromType: constantUtils.ACTIVITIES.ADMINS,
            action: constantUtils.ACTIVITIES.CREATE,
            toId: product._id,
            toType: constantUtils.ACTIVITIES.PRODUCTS,
        });
        await Category.findByIdAndUpdate(req?.body?.subCategoryId, {
            $push: {
                products: product._id,
            },
        });
    }
    await Partner.updateMany(
        {
            catalogueId: req?.body?.catalogueId,
            partnerType: constantUtils.STOCK_PARTNER,
            'availableProducts.productId': { $ne: product._id },
        },
        {
            $push: {
                availableProducts: {
                    productId: product._id,
                    stockQuantity:
                        product.productType === constantUtils.QUANTITY_STOCK
                            ? 0
                            : undefined,
                    status: constantUtils.ACTIVE,
                },
            },
        }
    );
    return res.json(product);
});

controller.getAllCatalogues = handler(async (req, res) => {
    let condition = {
        $or: helper.generateSearch(
            ['name', 'email', 'phone.number', 'phone.code'],
            req.body.search
        ),
    };

    // Sort
    let sort = '-_id';

    // Filters
    if (req?.body?.filters?.status)
        condition['status'] = req.body.filters.status;

    if (req?.body?.filters?.startDate)
        condition['createdAt'] = {
            $gte: req?.body.filters?.startDate,
        };

    if (req?.body?.filters?.endDate)
        condition['createdAt'] = {
            $lte: req?.body?.filters?.endDate,
        };

    const count = await Catalogue.count(condition);
    const catalogues = await Catalogue.find(condition)
        .select('name addedBy lastEdited lastEditedTime createdAt status')
        .populate(
            'lastEdited',
            'name data.firstName data.lastName data.email data.phone data.avatar'
        )
        .sort(sort)
        .skip(helper.getSkip(req))
        .limit(helper.getLimit(req))
        .lean();

    // Activity.create({
    //     fromId: req.user._id,
    //     fromType: constantUtils.ACTIVITIES.ADMINS,
    //     action: constantUtils.ACTIVITIES.VIEW_LIST,
    //     listType: constantUtils.SHOPS,
    // });

    return res.json({
        totalPages: helper.getTotalPages(req, count),
        totalDocs: count,
        currentPage: helper.getCurrentPage(req),
        data: catalogues,
    });
});

controller.shopCatalogueStructure = (shop) => ({
    id: shop?._id,
    shopName: shop?.shopName,
    avatar: shop?.avatar,
    catalogueContent: shop?.catalogueId?.categories
        // ?.filter(
        //     (each) =>
        //         each?.subCategories &&
        //         Array.isArray(each?.subCategories) &&
        //         each?.subCategories.length > 0
        // )
        ?.map((each) => ({
            id: each._id,
            name: each?.name,
            status: each?.status,
            image: each?.image,
            secondaryImage: each?.secondaryImage,
            contentType: 'CATEGORY',
            childrenType:
                each?.subCategories &&
                Array.isArray(each?.subCategories) &&
                each?.subCategories?.length > 0 &&
                each?.subCategories[0]?.byPassSubcategory
                    ? 'PRODUCT'
                    : 'SUBCATEGORY',
            children:
                each?.subCategories &&
                Array.isArray(each?.subCategories) &&
                each?.subCategories?.length > 0 &&
                each?.subCategories[0]?.byPassSubcategory
                    ? each?.subCategories[0]?.products?.map((eachProduct) =>
                          structureUtils.productStructure(eachProduct)
                      )
                    : each.subCategories
                          // ?.filter(
                          //     (eachChild) =>
                          //         eachChild?.products &&
                          //         Array.isArray(eachChild?.products) &&
                          //         eachChild?.products.length > 0
                          // )
                          ?.map((eachChild) => ({
                              id: eachChild._id,
                              contentType: 'SUBCATEGORY',
                              status: eachChild?.status,
                              name: eachChild?.name,
                              image: eachChild?.image,
                              secondaryImage: eachChild?.secondaryImage,
                              price: eachChild?.price,
                              description: eachChild?.description,
                              vegNonVeg: eachChild?.vegNonVeg,
                              childrenType: 'PRODUCT',
                              children: eachChild?.products?.map(
                                  (eachProduct) =>
                                      structureUtils.productStructure(
                                          eachProduct
                                      )
                              ),
                          })),
        })),
});

controller.getAllCatalogueNames = handler(async (req, res) => {
    const catalogues = await Catalogue.find({}).select('_id name');
    return res.json(catalogues);
});

controller.getCategories = handler(async (req, res) => {
    const categories = await Category.find({
        catalogueId: req?.body?.catalogueId,
        categoryType: 'CATEGORY',
    })
        .populate(
            'addedBy',
            'name data.firstName data.lastName data.email data.phone data.avatar'
        )
        .populate(
            'lastEdited',
            'name data.firstName data.lastName data.email data.phone data.avatar'
        );
    return res.json(categories);
});

controller.getSubCategories = handler(async (req, res) => {
    const categories = await Category.find({
        categoryId: req?.body?.categoryId,
        categoryType: 'SUBCATEGORY',
    })
        .populate(
            'addedBy',
            'name data.firstName data.lastName data.email data.phone data.avatar'
        )
        .populate(
            'lastEdited',
            'name data.firstName data.lastName data.email data.phone data.avatar'
        );
    return res.json(categories);
});

controller.getProducts = handler(async (req, res) => {
    const products = await Product.find({
        subCategoryId: req?.body?.subCategoryId,
    })
        .populate(
            'addedBy',
            'name data.firstName data.lastName data.email data.phone data.avatar'
        )
        .populate(
            'lastEdited',
            'name data.firstName data.lastName data.email data.phone data.avatar'
        );
    return res.json(products);
});

controller.deleteCategory = handler(async (req, res) => {
    await Category.deleteMany({
        $or: [
            {
                _id: req?.body?.id,
                categoryType: 'CATEGORY',
            },
            {
                categoryId: req?.body?.id,
                categoryType: 'SUBCATEGORY',
            },
        ],
    });
    await Product.deleteMany({
        categoryId: req?.body?.id,
    });
    await Catalogue.updateMany(
        { categories: { $in: [req?.body?.id] } },
        {
            $pull: {
                categories: req?.body?.id,
            },
        }
    );
    return res.json({
        message: 'Success',
    });
});

controller.toggleStatusCategory = handler(async (req, res) => {
    await Category.updateMany(
        {
            $or: [
                {
                    _id: req?.body?.id,
                    categoryType: 'CATEGORY',
                },
                {
                    categoryId: req?.body?.id,
                    categoryType: 'SUBCATEGORY',
                },
            ],
        },
        {
            $set: {
                status: req?.body?.status,
            },
        }
    );
    await Product.updateMany(
        {
            categoryId: req?.body?.id,
        },
        {
            status: req?.body?.status,
        }
    );
    return res.json({
        message: 'Success',
    });
});

controller.deleteSubCategory = handler(async (req, res) => {
    await Category.deleteMany({
        _id: req?.body?.id,
        categoryType: 'SUBCATEGORY',
    });
    await Category.updateMany(
        { subCategories: { $in: [req?.body?.id] } },
        {
            $pull: {
                subCategories: req?.body?.id,
            },
        }
    );
    await Product.deleteMany({
        subCategoryId: req?.body?.id,
    });
    return res.json({
        message: 'Success',
    });
});

controller.toggleStatusSubCategory = handler(async (req, res) => {
    await Category.updateMany(
        {
            _id: req?.body?.id,
            categoryType: 'SUBCATEGORY',
        },
        {
            $set: {
                status: req?.body?.status,
            },
        }
    );
    await Product.updateMany(
        {
            subCategoryId: req?.body?.id,
        },
        {
            $set: {
                status: req?.body?.status,
            },
        }
    );
    return res.json({
        message: 'Success',
    });
});

controller.deleteProduct = handler(async (req, res) => {
    await Product.deleteMany({
        _id: req?.body?.id,
    });
    await Category.updateMany(
        { products: { $in: [req?.body?.id] } },
        {
            $pull: {
                products: req?.body?.id,
            },
        }
    );
    return res.json({
        message: 'Success',
    });
});

controller.toggleStatusProduct = handler(async (req, res) => {
    await Product.updateMany(
        {
            _id: req?.body?.id,
        },
        {
            $set: {
                status: req?.body?.status,
            },
        }
    );
    return res.json({
        message: 'Success',
    });
});

controller.getCatalogueContents = handler(async (req, res) => {
    if (req?.body?.shopId) {
        const shop = await Shop.findById(req?.body?.shopId);
        req.body.id = shop?.catalogueId;
    }
    const catalogue = await Catalogue.findById(req?.body?.id).populate({
        path: 'categories',
        select: 'name status image',
        populate: {
            path: 'subCategories',
            select: 'name status image secondaryImage byPassSubcategory',
            populate: {
                path: 'products',
                select: 'name status image secondaryImage productType maxCartLimit price vegNonVeg tags rating',
            },
        },
    });
    // .populate('cities.cityId', '-location');
    return res.json(catalogue);
});

controller.getSingleShopSearch = handler(async (req, res) => {
    const config = await Other.config();

    if (config?.shop?.type === 'SINGLE' && config?.shop?.id) {
        const shop = await Shop.findById(config?.shop?.id).select(
            'catalogueId'
        );
        if (!shop || !shop?.catalogueId) return res.json([]);

        const [categories, products] = await parallel([
            function (callback) {
                Category.find({
                    $or: helper.generateSearch(['name'], req.body.search),
                    catalogueId: shop?.catalogueId,
                    categoryType: constantUtils.CATEGORY,
                    status: constantUtils.ACTIVE,
                })
                    .select('_id name image secondaryImage')
                    .lean()
                    .then((data) => {
                        callback(null, data);
                    })
                    .catch((err) => callback(null, []));
            },
            function (callback) {
                Product.find({
                    catalogueId: shop?.catalogueId,
                    $or: helper.generateSearch(['name'], req.body.search),
                    status: constantUtils.ACTIVE,
                })
                    .lean()
                    .then((data) => {
                        callback(null, data);
                    })
                    .catch((err) => callback(null, []));
            },
        ]);

        let dataToSend = [];
        if (categories && categories.length > 0)
            categories.forEach((each) =>
                dataToSend.push({
                    id: each._id,
                    name: each?.name,
                    image: each?.image,
                    type: constantUtils.CATEGORY,
                })
            );
        if (products && products.length > 0)
            products.forEach((each) =>
                dataToSend.push({
                    ...structureUtils.productStructure(each),
                    type: 'PRODUCT',
                })
            );

        return res.json(dataToSend);
    } else throw '400|' + constantUtils.DATA_NOT_FOUND;
});

controller.getSingleShopDetails = handler(async (req, res) => {
    const config = await Other.config();

    // Categories
    if (req?.body?.type === 'CATEGORIES') {
        if (config?.shop?.type === 'SINGLE' && config?.shop?.id) {
            const shop = await Shop.findById(config?.shop?.id)
                .select('shopName _id avatar')
                .populate({
                    path: 'catalogueId',
                    populate: {
                        path: 'categories',
                        match: {
                            status: constantUtils.ACTIVE,
                            'cities.cityId': req?.body?.cityId,
                        },
                        select: 'name status image secondaryImage',
                        populate: {
                            path: 'subCategories',
                            match: {
                                status: constantUtils.ACTIVE,
                                'cities.cityId': req?.body?.cityId,
                            },
                            select: 'name status image secondaryImage byPassSubcategory',
                            populate: {
                                path: 'products',
                                match: {
                                    status: constantUtils.ACTIVE,
                                    'cities.cityId': req?.body?.cityId,
                                },
                                select: 'name status image secondaryImage maxCartLimit price vegNonVeg description rating',
                            },
                        },
                    },
                })
                .lean();
            const structured = controller.shopCatalogueStructure(shop);
            return res.json(structured?.catalogueContent ?? []);
        } else throw '400|' + constantUtils.DATA_NOT_FOUND;

        // BEST SELLER
    } else if (req?.body?.type === 'BEST_SELLER') {
        if (config?.shop?.type === 'SINGLE' && config?.shop?.id) {
            const shop = await Shop.findById(config?.shop?.id)
                .select('shopName _id catalogueId')
                .lean();
            if (!(shop && shop?.catalogueId))
                throw '400|' + constantUtils.DATA_NOT_FOUND;
            else {
                const bestSellers = await Product.find({
                    catalogueId: shop?.catalogueId,
                    'cities.cityId': req?.body?.cityId,
                    tags: { $in: [constantUtils.BEST_SELLER] },
                    status: constantUtils.ACTIVE,
                }).select(
                    'name price description image secondaryImage maxCartLimit vegNonVeg rating'
                );
                return res.json(
                    bestSellers.map((each) =>
                        structureUtils.productStructure(each)
                    )
                );
            }
        } else throw '400|' + constantUtils.DATA_NOT_FOUND;

        // MUST TRY
    } else if (req?.body?.type === 'MUST_TRY') {
        if (config?.shop?.type === 'SINGLE' && config?.shop?.id) {
            const shop = await Shop.findById(config?.shop?.id)
                .select('shopName _id catalogueId')
                .lean();
            if (!(shop && shop?.catalogueId))
                throw '400|' + constantUtils.DATA_NOT_FOUND;
            else {
                const bestSellers = await Product.find({
                    catalogueId: shop?.catalogueId,
                    'cities.cityId': req?.body?.cityId,
                    tags: { $in: [constantUtils.MUST_TRY] },
                    status: constantUtils.ACTIVE,
                }).select(
                    'name price description image secondaryImage maxCartLimit vegNonVeg rating'
                );
                return res.json(
                    bestSellers.map((each) =>
                        structureUtils.productStructure(each)
                    )
                );
            }
        } else throw '400|' + constantUtils.DATA_NOT_FOUND;
    } else return res.json([]);
});

controller.getCatalogue = handler(async (req, res) => {
    const catalogue = await Catalogue.findById(req?.body?.id)
        .populate({
            path: 'categories',
            select: 'name status image',
            populate: {
                path: 'subCategories',
                select: 'name status image secondaryImage byPassSubcategory',
                populate: {
                    path: 'products',
                    select: 'name status image secondaryImage productType maxCartLimit price vegNonVeg tags',
                },
            },
        })
        .populate(
            'lastEdited',
            'name data.firstName data.lastName data.email data.phone data.avatar'
        )
        .populate('owners', 'shopName avatar')
        .populate(
            'addedBy',
            'name data.firstName data.lastName data.email data.phone data.avatar'
        )
        .lean();

    const shops = await Shop.find({
        catalogueId: req?.body?.id,
    })
        .select('_id shopName avatar')
        .lean();

    const partners = await Partner.find({
        catalogueId: req?.body?.id,
    })
        .select('_id firstName lastName avatar')
        .lean();

    return res.json({ ...catalogue, shops, partners });
});

controller.shopOnlineStatusChange = handler(async (req, res) => {
    const shop = await Shop.findById(req?.body?.id);
    if (!shop) throw '400|' + constantUtils.INVALID_ID;
    shop.onlineStatus = !shop?.onlineStatus ?? true;
    await shop.save();
    return res.json({
        message: 'SUCCESS',
    });
});

module.exports = controller;
