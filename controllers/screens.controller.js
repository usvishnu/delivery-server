const handler = require('express-async-handler');

const Screen = require('../models/screen.model');
const City = require('../models/city.model');
const Language = require('../models/language.model');
const Activity = require('../models/activity.model');
const Other = require('../models/other.model');
const Shop = require('../models/shop.model');

const constantUtils = require('../utils/constant.utils');
const structureUtils = require('../utils/structure.utils');
const fileUtils = require('../utils/upload.utils');

const helper = require('../utils/helper.utils');

const controller = {};

controller.getWalkthrough = handler(async (req, res) => {
    const walkThrough = await Screen.findOne({
        screenType: constantUtils.WALKTHROUGH,
    });

    if (!walkThrough || !walkThrough.data || !walkThrough.data.pages)
        throw '400|' + constantUtils.NOT_FOUND;

    return res.json(
        walkThrough.data.pages.map((each) => ({
            id: each.id,
            title: helper.getTranslation(
                each,
                'title',
                req?.user?.data?.languageCode ?? ''
            ),
            description: helper.getTranslation(
                each,
                'description',
                req?.user?.data?.languageCode ?? ''
            ),
            hexColor: each?.hexColor ?? '#FFFFFF',
            image: each.image,
        }))
    );
});

controller.getAllWalkThrough = handler(async (req, res) => {
    const walkThrough = await Screen.findOne({
        screenType: constantUtils.WALKTHROUGH,
    }).populate(
        'data.lastEdited',
        'data.firstName data.lastName data.email data.avatar _id data.hexColor'
    );
    if (walkThrough)
        Activity.create({
            fromId: req.user._id,
            fromType: constantUtils.ACTIVITIES.ADMINS,
            toId: walkThrough._id,
            toType: constantUtils.ACTIVITIES.SCREENS,
            action: constantUtils.ACTIVITIES.VIEW_WALKTHROUGH,
        });
    return res.json(
        { ...walkThrough?.data, updatedAt: walkThrough?.updatedAt } ?? {}
    );
});

controller.getMobileWalkThrough = handler(async (req, res) => {
    const ifLanguage = await Language.findOne({
        languageCode: req.headers['language-code'],
    }).select('languageName');

    if (!ifLanguage) throw '404|' + constantUtils.LANGUAGE_NOT_FOUND;
    else {
        const walkThrough = await Screen.findOne({
            screenType: constantUtils.WALKTHROUGH,
        });

        if (!walkThrough || !walkThrough.data || !walkThrough.data.pages)
            throw '400|' + constantUtils.NOT_FOUND;

        return res.json(
            walkThrough.data.pages.map((each) => ({
                title: helper.getTranslation(
                    each,
                    'title',
                    req.headers['language-code'] ?? 'en'
                ),
                description: helper.getTranslation(
                    each,
                    'description',
                    req.headers['language-code'] ?? 'en'
                ),
                hexColor: each?.hexColor ?? '#FFFFFF',
                image: each.image,
            }))
        );
    }
});

controller.uploadWalkThroughImage = handler(async (req, res) => {
    if (!req.file) throw '422|' + constantUtils.IMAGE_REQUIRED;
    const imageUrl = await fileUtils.upload(
        req.file.buffer,
        `SCREENS.WALKTHROUGH.${helper.generateRandom(5, '#')}.jpg`
    );
    return res.json(imageUrl);
});

controller.updateWalkThrough = handler(async (req, res) => {
    const updateData = {
        data: {
            pages: req.body.pages,
            lastEdited: req.user._id,
        },
    };

    const ifWalkThrough = await Screen.findOne({
        screenType: constantUtils.WALKTHROUGH,
    });

    let walkThrough;

    if (!ifWalkThrough)
        walkThrough = await Screen.create({
            screenType: constantUtils.WALKTHROUGH,
            ...updateData,
        });
    else
        walkThrough = await Screen.findOneAndUpdate(
            {
                screenType: constantUtils.WALKTHROUGH,
            },
            {
                $set: updateData,
            },
            {
                new: true,
            }
        );

    Activity.create({
        fromId: req.user._id,
        fromType: constantUtils.ACTIVITIES.ADMINS,
        toId: walkThrough._id,
        toType: constantUtils.ACTIVITIES.SCREENS,
        action: constantUtils.ACTIVITIES.UPDATE_WALKTHROUGH,
    });
    return res.json(walkThrough);
});

controller.updateHomeScreen = handler(async (req, res) => {
    const updateData = {
        data: {
            homeContentType: req?.body?.homeContentType,
            homeContents: req.body?.homeContents,
            lastEdited: req.user._id,
        },
    };

    const ifHomeScreen = await Screen.findOne({
        screenType: constantUtils.HOMESCREEN,
        'data.homeContentType': req?.body?.homeContentType,
    });

    let homeScreen;

    if (!ifHomeScreen)
        homeScreen = await Screen.create({
            screenType: constantUtils.HOMESCREEN,
            ...updateData,
        });
    else
        homeScreen = await Screen.findByIdAndUpdate(
            ifHomeScreen._id,
            {
                $set: updateData,
            },
            {
                new: true,
            }
        );

    Activity.create({
        fromId: req.user._id,
        fromType: constantUtils.ACTIVITIES.ADMINS,
        toId: homeScreen._id,
        toType: constantUtils.ACTIVITIES.SCREENS,
        action: constantUtils.ACTIVITIES.UPDATE_HOMESCREEN,
    });
    return res.json(homeScreen);
});

controller.getCityAndHomeScreen = handler(async (req, res) => {
    const config = await Other.config();
    const ifLanguage = await Language.findOne({
        languageCode: req?.headers['language-code'],
    }).select('languageName');

    if (!ifLanguage) throw '404|' + constantUtils.LANGUAGE_NOT_FOUND;
    else {
        const city = await City.findOne({
            location: {
                $geoIntersects: {
                    $geometry: {
                        type: 'Point',
                        coordinates: [req?.body?.lng ?? 0, req?.body?.lat ?? 0],
                    },
                },
            },
            status: constantUtils.ACTIVE,
        }).select(
            'locationName currencySymbol currencyCode timezone orderCalculation'
        );

        if (!city) throw '400|' + constantUtils.DELIVERY_UNAVAILABLE;

        const homeScreen = await Screen.findOne({
            screenType: constantUtils.HOMESCREEN,
            'data.homeContentType': 'SIGN_OUT',
        });

        let shop = null;

        if (config?.shop?.type === constantUtils.SINGLE) {
            shop = await Shop.findById(config?.shop?.id).select(
                'shopName email phone serviceAvailableTime onlineStatus'
            );
        }

        if (!homeScreen || !homeScreen.data || !homeScreen.data.homeContents)
            throw '400|' + constantUtils.NOT_FOUND;

        return res.json(
            structureUtils.userHomeScreenAndCityStructure(
                city,
                shop,
                homeScreen,
                req.headers['language-code'] ?? 'en'
            )
        );
    }
});

module.exports = controller;
