const handler = require('express-async-handler');
const mongoose = require('mongoose');

const Order = require('../models/order.model');
const Rating = require('../models/rating.model');

const structureUtils = require('../utils/structure.utils');
const helper = require('../utils/helper.utils');
const fileUtils = require('../utils/upload.utils');

const constantUtils = require('../utils/constant.utils');

const controller = {};

controller.rateOrder = handler(async (req, res) => {
    let ifOrder = await Order.findOne({
        _id: req?.body?.orderId,
    })
        .select('order user')
        .lean();

    if (!ifOrder || ifOrder.user.id?.toString() !== req?.user?._id?.toString())
        throw '400|' + constantUtils.INVALID_ID;
    else if (ifOrder?.order?.rating?.average)
        throw '400|' + constantUtils.ALREADY_RATED;

    if (req?.body?.products?.length)
        req?.body?.products?.map((each) => {
            // creating rating
            Rating.create({
                ratingFor: constantUtils.PRODUCT,
                userId: req?.user?._id,
                productId: each.productId,
                message: each?.message ?? undefined,
                rating: each?.rating,
                images: each?.images ?? undefined,
            }).then((data) =>
                // updating product average rating
                helper.updateRating(constantUtils.PRODUCT, each.productId)
            );
        });

    const updateObj = {};
    updateObj['order.rating.average'] = req?.body?.rating;
    if (req?.body?.message)
        updateObj['order.rating.message'] = req?.body?.message;

    await Order.findByIdAndUpdate(
        req?.body?.orderId,
        {
            $set: updateObj,
        },
        {
            new: true,
        }
    );
    return res.json({
        message: 'SUCCESS',
    });
});

controller.removeImage = handler(async (req, res) => {
    await fileUtils.delete(req?.body?.image);
    return res.json({
        message: constantUtils.SUCCESS,
    });
});

controller.ratePartner = handler(async (req, res) => {
    const order = await Order.findById(req?.body?.orderId)
        .select('user order partner')
        .lean();

    if (!order) throw '400|' + constantUtils.INVALID_ID;

    if (!order || order.user.id?.toString() !== req?.user?._id?.toString())
        throw '400|' + constantUtils.INVALID_ID;
    // else if (order?.order?.rating?.average)
    //     throw '400|' + constantUtils.ALREADY_RATED;

    Rating.create({
        ratingFor: constantUtils.PARTNER,
        userId: req?.user?._id,
        partnerId: req?.body?.partnerId,
        message: req?.body?.message ?? undefined,
        rating: req?.body?.rating,
        images: req?.body?.images ?? undefined,
    }).then((data) =>
        helper.updateRating(constantUtils.PARTNER, req?.body?.partnerId)
    );

    return res.json({
        message: 'SUCCESS',
    });
});

controller.rateUser = handler(async (req, res) => {
    const order = await Order.findById(req?.body?.orderId)
        .select('user order partner')
        .lean();

    if (!order) throw '400|' + constantUtils.INVALID_ID;

    if (!order || order.partner.id?.toString() !== req?.user?._id?.toString())
        throw '400|' + constantUtils.INVALID_ID;
    // else if (order?.order?.rating?.average)
    //     throw '400|' + constantUtils.ALREADY_RATED;

    Rating.create({
        ratingFor: constantUtils.USER,
        userId: req?.body?.userId,
        partnerId: req?.user?._id,
        message: req?.body?.message ?? undefined,
        rating: req?.body?.rating,
        images: req?.body?.images ?? undefined,
    }).then((data) =>
        helper.updateRating(constantUtils.USER, req?.body?.userId)
    );

    return res.json({
        message: 'SUCCESS',
    });
});

controller.uploadImage = handler(async (req, res) => {
    let imageUrl = '';
    if (req.file && req?.file?.buffer) {
        if (!req?.file?.mimetype.includes('image'))
            throw '400|' + constantUtils.INVALID_IMAGE;
        imageUrl = await fileUtils.upload(
            req?.file?.buffer,
            `${constantUtils.RATING_IMAGE}.${helper.generateRandom(
                20,
                '#'
            )}.jpg`
        );
    }
    return res.json({
        imageUrl,
    });
});

controller.getProductRatings = handler(async (req, res) => {
    const condition = {
        productId: mongoose.Types.ObjectId(req?.body?.productId),
    };
    const [count, ratings] = await Promise.all([
        Rating.count(condition),
        Rating.aggregate([
            {
                $match: condition,
            },
            {
                $sort: {
                    _id: -1,
                },
            },
            {
                $skip: helper.getSkip(req),
            },
            {
                $limit: helper.getLimit(req),
            },
            {
                $lookup: {
                    from: 'users',
                    localField: 'userId',
                    foreignField: '_id',
                    as: 'userId',
                },
            },
            {
                $unwind: '$userId',
            },
            {
                $project: {
                    _id: 0,
                    id: '$_id',
                    'user.firstName': '$userId.firstName',
                    'user.lastName': '$userId.lastName',
                    'user.avatar': '$userId.avatar',
                    message: 1,
                    images: 1,
                    rating: 1,
                    time: '$createdAt',
                },
            },
        ]),
    ]);

    return res.json({
        totalPages:
            helper.getTotalPages(req, count) === 0
                ? 1
                : helper.getTotalPages(req, count),
        totalDocs: count,
        currentPage: helper.getCurrentPage(req),
        data: ratings,
    });
});

module.exports = controller;
